<?php

/* @var $this yii\web\View */
use yii\helpers\html;
use circulon\widgets\ColumnListView;

/*
 * 
 *      Vista de la pagina de inicio de la aplicacion
 *      Muestra enlaces a los ultimos 4 eventos y a las acciones basicas del usuario
 * 
 */

$this->title = 'Lixet';

// Se obtiene el rol del usuario registrado para mostrarle sus acciones permitidas
$rol = isset(array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]) ? array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]:'';
$misentradas = "entradas/misentradas";
$migrupo = "jefes/migrupo";
$misclientes = "clientes/misclientes";
$miseventos = "eventos/miseventos";
if($rol=="Admin"){
    // Si es administrador los enlaces le redirigen a los index de los controladores mostrados
    $misentradas = "entradas/index";
    $migrupo = "jefes/grupo";
    $misclientes = "clientes/index";
    $miseventos = "eventos/index";
}
?>
<div class="site-index">

    <!-- Banner header -->
    <div id="banner" class="p-5 text-center bg-image d-none">
        <div class="d-flex justify-content-center align-items-center h-100">
            <div class="text-white justify-content-center align-items-center">
              <h1 class="mb-3 sombra txt-banner"><?= $this->title ?></h1>
            </div>
        </div>
    </div>
    <!-- Banner header -->
    
    
    <div class="body-content">
        
        <!-- Seccion 1 -->
        <div class="jumbotron text-center bg-transparent nomb">
            <h1 class="display-4">Últimos eventos</h1>
        </div>
        
        <div class="row nomargin">
            <!-- Muestra los ultimos 4 eventos dados de alta en la aplicacion -->
            <?= ColumnListView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n",
                'columns' => 4,
                'itemOptions' => [
                    'class' => 'col-md-3 nopadding boxes-content',
                ],
                'itemView' => '_evento',
            ]);
            ?>
            
        </div>
        <!-- Seccion 1 -->
       
        <!-- Seccion 2 -->
        <div class="jumbotron text-center bg-dark text-white mb-5">
            
            <h1 class="display-4">¡Empieza a vender!</h1>
            <p>Todas las herramientas que necesitas</p>
            <hr class="separator-sm">
            
            <!-- Acciones basicas de los vendedores y jefes (a los administradores les redirige a los index)-->
            <div class="card-deck flex-row text-center">
            
                <a href="<?=$misentradas?>" class="card bg-transparent text-white shadow-lightblue home-card">
                    <?= Html::img("@web/img/home/tickets-tarjeta.jpg", ['class' => 'card-img', 'alt' => 'Imagen no encontrada']) ?>
                    <div class="card-img-overlay centrar sombra">
                      <h5 class="card-title"><?=$rol=="Admin" ? 'Entradas' : 'Mis entradas'?></h5>
                    </div>
                </a>

                <a href="<?=$migrupo?>" class="card bg-transparent text-white shadow-lightblue home-card">
                    <?= Html::img("@web/img/home/grupos-tarjeta.jpg", ['class' => 'card-img', 'alt' => 'Imagen no encontrada']) ?>
                    <div class="card-img-overlay centrar sombra">
                      <h5 class="card-title"><?=$rol=="Admin" ? 'Grupos' : 'Mi grupo'?></h5>
                    </div>
                </a>

                <a href="<?=$misclientes?>" class="card bg-transparent text-white shadow-lightblue home-card">
                    <?= Html::img("@web/img/home/rrpps-tarjeta.jpg", ['class' => 'card-img', 'alt' => 'Imagen no encontrada']) ?>
                    <div class="card-img-overlay centrar sombra">
                      <h5 class="card-title"><?=$rol=="Admin" ? 'Clientes' : 'Mis Clientes'?></h5>
                    </div>
                </a>

                <a href="<?=$miseventos?>" class="card bg-transparent text-white shadow-lightblue home-card">
                    <?= Html::img("@web/img/home/eventos-tarjeta.jpg", ['class' => 'card-img', 'alt' => 'Imagen no encontrada']) ?>
                    <div class="card-img-overlay centrar sombra">
                      <h5 class="card-title"><?=$rol=="Admin" ? 'Eventos' : 'Mis eventos'?></h5>
                    </div>
                </a>
            
            </div>
        
        </div>
        <!-- Seccion 2 -->
        
    </div>
</div>
