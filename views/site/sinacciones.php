<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

/*
 * 
 *      Vista que se muestra unicamente a los usuarios que no tengan asignados roles
 *      Muestra indicaciones de como proceder
 * 
 */

$this->title = '¿Por que no veo las acciones básicas?';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        En Lixet nos preocupamos mucho por la seguridad, por lo que el administrador además de crear un usuario debe agregarle permisos para poder navegar en la aplicación.
        <br>
        Si estas viendo esta página <b>ponte en contacto con tu jefe de ventas para que le comunique al administrador que te asigne los permisos necesarios.</b>
        <br>
        <p class="text-danger"><b><?=$info?></b></p>
    </p>

</div>