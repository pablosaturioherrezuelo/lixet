<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Html;
$this->title = 'Manage - Lixet';

/*
 * 
 * Vista oculta, SOLO PARA MANTENIMIENTOS.
 * Muestra enlaces a todos los cruds
 * Para el usuario no existe
 * 
 */

?>

<div class="site-index">

    <div class="header jumbotron text-center bg-transparent">
        <h1 class="display-4">Manage</h1>
        <p class="lead">Todos los cruds de la aplicación.</p>
    </div>
    
    <div class="body-content">

        <div class="card-deck flex-row text-center">
            
            <div class="card border-lxt">
                <?= Html::img("@web/img/manage/alquileres.png", ['class' => 'card-img-top', 'alt' => 'Imagen no encontrada']) ?>
                <div class="card-body">
                    <h5 class="card-title">Alquileres</h5>
                    <?= Html::a('Ver', ['alquileres/index'], ['class' => 'btn btn-lxt']) ?>
                </div>
            </div>
            
            <div class="card border-lxt">
                <?= Html::img("@web/img/manage/clientes.png", ['class' => 'card-img-top', 'alt' => 'Imagen no encontrada']) ?>
                <div class="card-body">
                    <h5 class="card-title">Clientes</h5>
                    <?= Html::a('Ver', ['clientes/index'], ['class' => 'btn btn-lxt']) ?>
                </div>
            </div>
            
            <div class="card border-lxt">
                <?= Html::img("@web/img/manage/contratos.png", ['class' => 'card-img-top', 'alt' => 'Imagen no encontrada']) ?>
                <div class="card-body">
                    <h5 class="card-title">Contratos</h5>
                    <?= Html::a('Ver', ['contratos/index'], ['class' => 'btn btn-lxt']) ?>
                </div>
            </div>
            
            <div class="card border-lxt">
                <?= Html::img("@web/img/manage/entradas.png", ['class' => 'card-img-top', 'alt' => 'Imagen no encontrada']) ?>
                <div class="card-body">
                    <h5 class="card-title">Entradas</h5>
                    <?= Html::a('Ver', ['entradas/index'], ['class' => 'btn btn-lxt']) ?>
                </div>
            </div>
            
            <div class="card border-lxt">
                <?= Html::img("@web/img/manage/eventos.png", ['class' => 'card-img-top', 'alt' => 'Imagen no encontrada']) ?>
                <div class="card-body">
                    <h5 class="card-title">Eventos</h5>
                    <?= Html::a('Ver', ['eventos/index'], ['class' => 'btn btn-lxt']) ?>
                </div>
            </div>
            
        </div>
        
        <hr style="width: 1px">
        
        <div class="card-deck flex-row text-center">
            
            <div class="card border-lxt">
                <?= Html::img("@web/img/manage/jefes.png", ['class' => 'card-img-top', 'alt' => 'Imagen no encontrada']) ?>
                <div class="card-body">
                    <h5 class="card-title">Jefes</h5>
                    <?= Html::a('Ver', ['jefes/index'], ['class' => 'btn btn-lxt']) ?>
                </div>
            </div>
            
            <div class="card border-lxt">
                <?= Html::img("@web/img/manage/recintos.png", ['class' => 'card-img-top', 'alt' => 'Imagen no encontrada']) ?>
                <div class="card-body">
                    <h5 class="card-title">Recintos</h5>
                    <?= Html::a('Ver', ['recintos/index'], ['class' => 'btn btn-lxt']) ?>
                </div>
            </div>
            
            <div class="card border-lxt">
                <?= Html::img("@web/img/manage/rrpps.png", ['class' => 'card-img-top', 'alt' => 'Imagen no encontrada']) ?>
                <div class="card-body">
                    <h5 class="card-title">RRPPS</h5>
                    <?= Html::a('Ver', ['rrpps/index'], ['class' => 'btn btn-lxt']) ?>
                </div>
            </div>
            
            <div class="card border-lxt">
                <?= Html::img("@web/img/manage/tipos-entradas.png", ['class' => 'card-img-top', 'alt' => 'Imagen no encontrada']) ?>
                <div class="card-body">
                    <h5 class="card-title">Tipos de entradas</h5>
                    <?= Html::a('Ver', ['tipos-entradas/index'], ['class' => 'btn btn-lxt']) ?>
                </div>
            </div>
            
            <div class="card border-0">
            </div>
            
        </div>
        
    </div>
    
</div>