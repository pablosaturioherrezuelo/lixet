<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

/*
 * 
 *  Vista de inicio de sesion de usuarios
 * 
 */

$this->title = 'Iniciar Sesión';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login text-center mt-4">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Plataforma de gestión de entradas físicas de eventos</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{input}\n{error}",
            //'labelOptions' => ['class' => 'col-lg-1 col-form-label mr-lg-3 text-center'],
            'labelOptions' => ['class' => 'col-lg-12 col-form-label mr-lg-12'],
            'inputOptions' => ['class' => 'col-md-12 form-control'],
            'errorOptions' => ['class' => 'col-lg-12 invalid-feedback'],
        ],
    ]); ?>
    
    <div class="row">
        <div class="col-md-3">
            
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
        </div>
        <div class="col-md-3">
            
        </div>
        <div class="col-md-3">
            
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'password')->passwordInput() ?>
        </div>
        <div class="col-md-3">
            
        </div>
        <div class="col-md-3">
            
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'rememberMe')->checkbox([
                'template' => "<div class=\"col-lg-12 custom-control custom-checkbox\">{input} {label}</div>\n<div class=\"col-lg-12\">{error}</div>",
                //'template' => "<div class=\"offset-lg-1 col-lg-3 custom-control custom-checkbox\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
            ]) ?> 
        </div>
        
    </div>

        <div class="form-group">
            <!--<div class="offset-lg-1 col-lg-11">-->
            <div class="">
                <?= Html::submitButton('Iniciar Sesión', ['class' => 'btn btn-lxt', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>

    <!--<div class="offset-lg-1" style="color:#999;">
        You may login with <strong>admin/admin</strong> or <strong>demo/demo</strong>.<br>
        To modify the username/password, please check out the code <code>app\models\User::$users</code>.
    </div>-->
</div>
