<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Entradas */

/*
 * 
 *  No usar, es una prueba
 * 
 */

$this->title = 'Generar entradas';
?>
<div class="entradas-create">

    <?= $form->field($model, 'amount_paid', [
    'addon' => [ 
        'prepend' => ['content' => '$', 'options'=>['class'=>'alert-success']],
        'append' => ['content' => '.00', 'options'=>['style' => 'font-family: Monaco, Consolas, monospace;']],
    ]
])?>
    
</div>