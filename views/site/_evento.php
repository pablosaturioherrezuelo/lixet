<?php
use yii\helpers\Html;

/*
 * 
 *  Vista de un evento en la pagina de inicio
 * 
 */

// Si el usuario no esta registrado no se le deja acceder al enlace
if(!Yii::$app->user->isGuest){
    $link = 'eventos/view/';
    $id = $model->id;
    $mensaje = $model->nombre ." <br> ". $model->edicion;
} else {
    $link = '';
    $id = '';
    $mensaje = 'Inicie sesion para operar';
}

// Busca la imagen asociada al evento y en su defecto pone una estandar
if(file_exists(getcwd().'/img/eventos/'.$model->codigo.'-home.jpg')){
    $img = '@web/img/eventos/' . $model->codigo .'-home.jpg';
} else if(file_exists(getcwd().'/img/eventos/'.$model->codigo.'-home.png')){
    $img = '@web/img/eventos/' . $model->codigo .'-home.png';
} else {
    $img =  '@web/img/eventos/default-home.jpg';
}
?>

<div class="pbottom">
    <?= Html::a(Html::img($img, ['alt' => 'Imagen no encontrada', 'class' => 'box-img']),
            [$link, 'id' => $id]) ?>
    
    <?= Html::a($mensaje, [$link, 'id' => $id], ['class'=>'boxes-text']) ?>
</div>           