<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Entradas */

/*
 * 
 *      Vista detallada de una entrada cargada desde una vista de un evento
 * 
 */

$this->title = 'Entrada: ' . $model->nomevento . ' - '. $model->numero;

$this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['eventos/index']];
$this->params['breadcrumbs'][] = ['label' => $model->nomevento, 'url' => ['eventos/view', 'id' => $model->evento]];
$this->params['breadcrumbs'][] = ['label' => 'Entradas', 'url' => ['indexfromeventos', 'evento' => $model->evento, 'nomevento' => $model->nomevento]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="entradas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['updatefromeventos', 'id' => $model->id], ['class' => 'btn btn-lxt']) ?>
        <?= Html::a('Borrar', ['deletefromeventos', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas seguro de querer borrar este registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>    
    
    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-hover table-responsive-md'],
        'attributes' => [
            'nomevento',
            'numero',
            //'precio',
            [
                'label' => 'Precio',
                'attribute' =>'precio',
                'value'=>function ($model) {
                    return number_format($model->precio, 2, ',', '.') . "€";
                },
            ],
            [
                'label' => 'Comisión',
                'attribute' =>'comision',
                'value'=>function ($model) {
                    return number_format($model->comision, 2, ',', '.') . "€";
                },
            ],
            'tipo',
            'nomvendedor',
            'nomcomprador',
            'fech',
            //'hora',
        ],
    ]) ?>

</div>
