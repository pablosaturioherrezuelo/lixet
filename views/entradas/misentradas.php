<?php
use yii\helpers\Html;
use yii\helpers\HTMLPurifier;
use circulon\widgets\ColumnListView;
$titulo = "Mis Entradas";
$this->title = $titulo;
$this->params['breadcrumbs'][] = $titulo;

/*
 * 
 *      Vista que muestra las entradaas que tenga asignadas el usuario registrado
 * 
 */

?>

<div class="well well-sm text-center">
    <h1 style="">
        <?=$titulo?>
    </h1>
</div>

<div class="">
    <?= ColumnListView::widget([ 
        'dataProvider' => $dataProvider,
        'itemView' => '_misentradas',
        //'class' => 'col-sm-3',
        'layout'=> "\n{pager}\n{items}",
        'columns' => 2,
    ]);
    ?>
</div>