<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

/*
 *
 *      Vista para mostrar los mejores vendedores de un evento
 * 
 */

// Se obtiene el rol del usuario registrado para mostrarle sus acciones permitidas
$nomevento = Yii::$app->getRequest()->getQueryParam('nomevento');
$evento = Yii::$app->getRequest()->getQueryParam('evento');

// Migas de pan para volver al evento al que pertenece
$this->title = 'Mejores Vendedores de ' . $nomevento;
$this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['eventos/index']];
$this->params['breadcrumbs'][] = ['label' => $nomevento, 'url' => ['eventos/view','id'=>$evento]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entradas-index">

    <h1><?= Html::encode($this->title)?></h1>
    <h5><?=$total?></h5>

   <?= GridView::widget([
        
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-hover table-responsive-md',],
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nomvendedor',
            'cantidad',
            //'comision',
            //'precio',
            //'beneficio',
            [
                'label' => 'Comisión',
                'attribute' =>'comision',
                'contentOptions' => ['class' => 'text-left'],
                'value'=>function ($model) {
                    return number_format($model->comision, 2, ',', '.') . "€";
                },
            ],
            [
                'label' => 'Precio',
                'attribute' =>'precio',
                'contentOptions' => ['class' => 'text-left'],
                'value'=>function ($model) {
                    return number_format($model->precio, 2, ',', '.') . "€";
                },
            ],
            [
                'label' => 'Beneficio',
                'attribute' =>'beneficio',
                'contentOptions' => ['class' => 'text-left'],
                'value'=>function ($model) {
                    return number_format($model->beneficio, 2, ',', '.') . "€";
                },
            ],
        ],
    ]); ?>


</div>
