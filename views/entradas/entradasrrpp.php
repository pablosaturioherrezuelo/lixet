<?php
use yii\helpers\Html;
use yii\helpers\HTMLPurifier;
use circulon\widgets\ColumnListView;

/*
 * 
 *      Vista para mostrar las entradas asignadas a un RRPP en un evento
 * 
 */

// Recibe los parametros enviados al controlador
$nomvendedor = Yii::$app->getRequest()->getQueryParam('nomrrpp');
$nomevento = Yii::$app->getRequest()->getQueryParam('nomevento');
$rrpp = Yii::$app->getRequest()->getQueryParam('rrpp');
$evento = Yii::$app->getRequest()->getQueryParam('evento');

$titulo = "Entradas de " . $nomvendedor . " en " . $nomevento;
$this->title = $titulo;
$this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['eventos/index']];
$this->params['breadcrumbs'][] = ['label' => $nomevento, 'url' => ['eventos/view', 'id' => $evento]];
$this->params['breadcrumbs'][] = ['label' => 'RRPPS', 'url' => ['rrpps/rrpps', 'evento' => $evento, 'nomevento' => $nomevento]];
$this->params['breadcrumbs'][] = $nomvendedor;
?>

<div class="well well-sm text-center">
    <h2 style="">
        <?=$titulo?>
    </h2>
</div>

<p>
    <?= Html::a('Asignar entradas', ['/entradas/asignarentradas', 'evento' => $evento, 'rrpp' => $rrpp, 'nomevento' => $nomevento, 'nomrrpp' => $nomvendedor], ['class' => 'btn btn-lxt sombrabox']) ?>
</p>

<div class="">
    <?= ColumnListView::widget([ 
        'dataProvider' => $dataProvider,
        'itemView' => '_entradarrpp',
        'layout'=> "\n{pager}\n{items}",
        'columns' => 2,
    ]);
    ?>
</div>