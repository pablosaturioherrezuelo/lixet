<?php
use yii\helpers\Html;
use yii\helpers\HTMLPurifier;
use circulon\widgets\ColumnListView;

/*
 * 
 *      Vista que muestra las entradas que tenga asignadas el usuario registrado en un evento recibido como parametro
 * 
 */

$nomevento = Yii::$app->getRequest()->getQueryParam('nomevento');
$titulo = "Mis Entradas de " . $nomevento;
$this->title = $titulo;
$this->params['breadcrumbs'][] = ['label' => 'Mis Eventos', 'url' => ['eventos/miseventos']];
$this->params['breadcrumbs'][] = $titulo;
?>

<div class="well well-sm text-center">
    <h1 style="">
        <?=$titulo?>
    </h1>
</div>

<div class="">
    <?= ColumnListView::widget([ 
        'dataProvider' => $dataProvider,
        'itemView' => '_misentradaseventos',
        'layout'=> "\n{pager}\n{items}",
        'columns' => 2,
    ]);
    ?>
</div>