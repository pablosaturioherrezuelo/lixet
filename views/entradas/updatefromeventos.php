<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Entradas */

/*
 * 
 *      Vista que permite actualizar una entrada de un evento recibido como parametro
 * 
 */

$this->title = $model->evento0->nombre .' (' .$model->evento0->codigo ."-". $model->evento0->edicion . ")";

$this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['eventos/index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['eventos/view', 'id' => $model->evento]];
$this->params['breadcrumbs'][] = ['label' => 'Entradas', 'url' => ['indexfromeventos', 'evento' => $model->evento, 'nomevento' => $model->nomevento]];
$this->params['breadcrumbs'][] = ['label' => 'Entrada: ' . $model->numero, 'url' => ['viewfromeventos', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';

$this->title = 'Actualizar entrada: ' . $this->title . " - " . $model->numero;
?>
<div class="entradas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formcrearentrada', [
        'model' => $model,
    ]) ?>

</div>
