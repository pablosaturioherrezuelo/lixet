<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

/*
 * 
 *  Vista principal para asignar entradas a un vendedor contratado en un evento
 * 
 */

// Se obtiene el rol del usuario registrado para mostrarle sus acciones permitidas
$nomevento = Yii::$app->getRequest()->getQueryParam('nomevento');
$evento = Yii::$app->getRequest()->getQueryParam('evento');
$nomrrpp = Yii::$app->getRequest()->getQueryParam('nomrrpp');
$rrpp = Yii::$app->getRequest()->getQueryParam('rrpp');

$this->title = 'Asignar entradas';
// Migas de pan para volver al vendedor al que se le van a asignar las entradas y al evento al que pertenece
$this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['eventos/index']];
$this->params['breadcrumbs'][] = ['label' => $nomevento, 'url' => ['eventos/view', 'id' => $evento]];
$this->params['breadcrumbs'][] = ['label' => 'RRPPS', 'url' => ['rrpps/rrpps', 'evento' => $evento, 'nomevento' => $nomevento]];
$this->params['breadcrumbs'][] = ['label' => $nomrrpp, 'url' => ['entradas/entradasrrpp', 'rrpp' => $rrpp, 'nomrrpp' => $nomrrpp, 'evento' => $evento, 'nomevento' => $nomevento]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entradas-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= GridView::widget([
        
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-hover table-responsive-md',],
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            'nomevento',
            'numentrada',
            //'numero',
            //'precio',
            [
                'label' => 'Precio',
                'attribute' =>'precio',
                'contentOptions' => ['class' => 'text-right'],
                'value'=>function ($model) {
                    return number_format($model->precio, 2, ',', '.') . "€";
                },
            ],
            'tipo',
            //'fecha',
            //'nomcomprador',
            'nomvendedor',
            
            [
                'header' => '',
                'content' => function($model) use ($rrpp,$nomrrpp) {
                    return Html::a('Cambiar',['updatefromentradasrrpp','id'=>$model->id, 'rrpp' => $rrpp, 'nomrrpp' => $nomrrpp], ['class' => 'btn btn-lxt']);
                }  
            ],
        ],
    ]); ?>


</div>
