<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Entradas */

/*
 * 
 *      Vista para vender una entrada desde un evento
 * 
 */

$nomevento = $model->nomevento;
$evento = $model->evento;

$this->title = $model->evento0->nombre .' '. $model->evento0->edicion . " - ". $model->numero;

// Obtenemos el rol para cambiar las migas de pan en funcion de las acciones permitidas del usuario
$rol = isset(array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]) ? array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]:'';
if($rol == "Jefe") {
    $this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['eventos/index']];
    $this->params['breadcrumbs'][] = ['label' => $nomevento, 'url' => ['eventos/view','id'=>$evento]];
    $this->params['breadcrumbs'][] = ['label' => 'Entradas', 'url' => ['entradas/indexfromeventos','evento' => $evento, 'nomevento' => $nomevento]]; 
} else {
    $this->params['breadcrumbs'][] = ['label' => 'Mis Entradas', 'url' => ['entradas/misentradaseventos','evento' => $evento, 'nomevento' => $nomevento]];
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entradas-update">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <!--  Advertencia sobre la venta de una entrada -->
    <div class="col-md-12 bg-danger text-white mt-4 mb-4 pt-4 pl-4 pr-4 pb-2 h-auto cerrar">
        <span class="closebtn">&times;</span>  
        <p><b>Recuerda que al pulsar sobre el botón guardar se confirmará la transacción</b> y deberas recibir <?=number_format($model->precio, 2, ',', '.')?>€ por parte del cliente.
        <br>
        Recibirás <?=number_format($model->comision, 2, ',', '.')?>€ por parte del jefe de tu equipo de ventas dentro del plazo previamente acordado.
        <br>
        En caso de no haber seleccionado ningun comprador la transacción se considerará nula y no se realizaran cambios.
        </p>
    </div>   
    
    <!--  Codigo javascript para mostrar la advertencia -->
    <script>
        var close = document.getElementsByClassName("closebtn");
        var i;

        for (i = 0; i < close.length; i++) {
          close[i].onclick = function(){
            var div = this.parentElement;
            div.style.opacity = "0";
            setTimeout(function(){ div.style.display = "none"; }, 600);
          }
        }
    </script>
    

    <?= $this->render('_formvenderentrada', [
        'model' => $model,
    ]) ?>

</div>
