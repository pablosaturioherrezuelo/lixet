<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/*
 * 
 *      Vista que muestra una entrada que tenga asignada el usuario registrado
 * 
 */

// Busca la imagen asignada al evento al que pertenece y en su defecto muestra una predeterminada
if(file_exists(getcwd().'/img/eventos/'.$model->codigo.'-home.jpg')){
    $img = '@web/img/eventos/' . $model->codigo .'-home.jpg';
} else if(file_exists(getcwd().'/img/eventos/'.$model->codigo.'-home.png')){
    $img = '@web/img/eventos/' . $model->codigo .'-home.png';
} else {
    $img =  '@web/img/eventos/default-home.jpg';
}
$target = '_self';

// Si la entrada tiene comprador asignado se mostraran las acciones para imprimirla. En caso contrario permitira venderla
if($model->nomcomprador){
    $var= number_format($model->precio, 2, ',', '.') . "€ <br>" . $model->nomcomprador;
    $texto = 'Imprimir';
    $link = 'report';
    $color = 'btn-danger';
    $linkt = $link.'?id='.$model->id;
    $tbg = 't-bgdanger';
    $target = '_blank';
} else {
    $var = number_format($model->precio, 2, ',', '.') . "€";
    $texto = 'Vender';
    $link = 'venderentrada';
    $color = 'btn-lxt';
    $linkt = $link.'?id='.$model->id;
    $tbg = 't-bgdark';
}
$button = Html::a($texto, [$link,'id'=>$model->id], ['class' => "btn $color sombrabox mt-0",'target'=>$target]);
?>

<a href="<?=$linkt?>" target="<?=$target?>" class="card entrada sombrabox bg-dark text-white" style="margin-top: 10px; margin-bottom: 20px">
    <?= Html::img($img, ['alt' => 'Imagen no encontrada', 'class' => 'card-img'])?>
    <div class="<?=$tbg?> card-img-overlay"></div> 
    <div class="caption text-center card-img-overlay">
        <h6 class="card-title"><?= $model->nomevento ?></h6>
        <p class="card-text"><?= $model->numero . " - " . $model->tipo . "<br>" . $var?></p>
    </div>
</a> 
<div class="text-center pb-2">
    <?= $button ?>
</div>

