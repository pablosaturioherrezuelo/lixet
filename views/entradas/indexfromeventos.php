<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

/*
 * 
 *  Vista principal de las entradas de un evento
 * 
 */

// Se obtiene el rol del usuario registrado para mostrarle sus acciones permitidas
$nomevento = Yii::$app->getRequest()->getQueryParam('nomevento');
$evento = Yii::$app->getRequest()->getQueryParam('evento');

$this->title = 'Entradas';

// Migas de pan para volver al evento al que pertenece
$this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['eventos/index']];
$this->params['breadcrumbs'][] = ['label' => $nomevento, 'url' => ['eventos/view', 'id' => $evento]];
$this->params['breadcrumbs'][] = $this->title;
$this->title = $this->title . '  '. $nomevento;

// Se obtiene el rol del usuario registrado para mostrarle sus acciones permitidas
$rol = isset(array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]) ? array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]:'';
$permiso=0;
$crear = "";
// Por defecto solo se permite ir a las entradas que tenga asignado el usuario
$actioncolumn = [
                'header' => '',
                'content' => function($model) {
                    return Html::a('Mis Entradas',['misentradas'], ['class' => 'btn btn-lxt']);
                }  
            ];
switch ($rol) {
    case "Admin":
        // Permisos totales
        $permiso = 4;
        $crear = Html::a('Crear entradas', ['generar','evento'=>$evento,'nomevento'=>$nomevento], ['class' => 'btn btn-lxt']);
        $actioncolumn = [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    if ($action == "view") {
                        return Url::to(['viewfromeventos', 'id' => $model->id]);
                    } else if ($action == "update"){
                         return Url::to(['updatefromeventos', 'id' => $model->id]);
                    } else if ($action == "delete"){
                         return Url::to(['deletefromeventos', 'id' => $model->id]);
                    }
                 }
            ];
        break;
    case "Jefe":
        $permiso = 3;
        // Ver sus entradas asignadas, vender la entrada de un RRPP o imprimir una entrada vendida
        $crear = Html::a('Mis entradas', ['entradas/misentradaseventos','evento'=> $evento,'nomevento'=>$nomevento], ['class' => 'btn btn-lxt']);
        $actioncolumn = [
                'header' => '',
                'content' => function($model) {
                    if($model->nomcomprador){
                        return Html::a('Imprimir',['report','id'=>$model->id], ['class' => 'btn btn-danger']);
                    } else {
                        return Html::a('Vender',['venderentrada','id'=>$model->id], ['class' => 'btn btn-lxt']);
                    }
                }  
            ];
        break;
    case "RRPP":
        $permiso = 2;
        break;
    case "Cliente":
        $permiso = 1;
        break;
}
?>
<div class="entradas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= $crear ?>
    </p>


    <?= GridView::widget([
        
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-hover table-responsive-md',],
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => [
            'nomevento',
            'numentrada',
            //'precio',
            [
                'label' => 'Precio',
                'attribute' =>'precio',
                'contentOptions' => ['class' => 'text-center'],
                'value'=>function ($model) {
                    return number_format($model->precio, 2, ',', '.') . "€";
                },
            ],
            'tipo',
            'nomvendedor',
            'nomcomprador',
            'fecha',
            $actioncolumn,
            
        ],
    ]); ?>


</div>
