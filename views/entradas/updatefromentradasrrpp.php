<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Entradas */

/*
 * 
 *      Vista que permite asignar una entrada a un rrpp recibido como parametro
 * 
 */

$nomevento = $model->nomevento;
$evento = $model->evento;
// Obtiene los parametros del controlador
$nomrrpp = Yii::$app->getRequest()->getQueryParam('nomrrpp');
$rrpp = Yii::$app->getRequest()->getQueryParam('rrpp');

$this->title = $model->evento0->nombre .' '. $model->evento0->edicion . " - ". $model->numero;

// Migas de pan para volver al evento y al rrpp al que pertenece
$this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['eventos/index']];
$this->params['breadcrumbs'][] = ['label' => $nomevento, 'url' => ['eventos/view', 'id' => $evento]];
$this->params['breadcrumbs'][] = ['label' => 'RRPPS', 'url' => ['rrpps/rrpps', 'evento' => $evento, 'nomevento' => $nomevento]];
$this->params['breadcrumbs'][] = ['label' => $nomrrpp, 'url' => ['entradas/entradasrrpp', 'rrpp' => $rrpp, 'nomrrpp' => $nomrrpp, 'evento' => $evento, 'nomevento' => $nomevento]];
$this->params['breadcrumbs'][] = ['label' => 'Asignar entradas', 'url' => ['asignarentradas', 'rrpp' => $rrpp, 'nomrrpp' => $nomrrpp,
    'evento' => $evento, 'nomevento' => $nomevento]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entradas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formcambiarvendedor', [
        'model' => $model,
    ]) ?>

</div>
