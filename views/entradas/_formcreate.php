<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Entradas */
/* @var $form yii\widgets\ActiveForm */

/*
 * 
 *      Formulario para generar entradas
 * 
 */

?>

<div class="entradas-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        
        <div class="col-md-6">
            
            <?= $form->field($model, 'evento')->dropDownList($model->getdropdownEventos(),['prompt' => 'Seleccione una opción']) ?>
            
        </div>
        
        <div class="col-md-3">
            
            <?= $form->field($model, 'cantidad')->textInput() ?>
        
        </div>
        
        <div class="col-md-3">
            
            <?= $form->field($model, 'tipo')->textInput() ?>
            
        </div>
        
        
        <div class="col-md-3">

            <?= $form->field($model, 'precio')->textInput() ?>
            
            <?= $form->field($model, 'comision')->textInput() ?>
            
        </div>
        
        <div class="col-md-3">
            
            <?= $form->field($model, 'fecha')->input('date',['readonly' => 'true'])?>

            <?= $form->field($model, 'hora')->input('time',['readonly' => 'true']   )?>
            
        </div>
        
        <div class="col-md-6">
            
            <?= $form->field($model, 'vendedor')->dropDownList($model->getdropdownRrpps(),['prompt' => '','disabled' => 'disabled']) ?>
            
            <?= $form->field($model, 'comprador')->dropDownList($model->getdropdownClientes(),['prompt' => '','disabled' => 'disabled']) ?>
            
        </div>
        
    </div>

    <!--<?= $form->field($model, 'vendedor')->textInput() ?>-->
    <!--<?= $form->field($model, 'comprador')->textInput() ?>-->

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-lxt']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
