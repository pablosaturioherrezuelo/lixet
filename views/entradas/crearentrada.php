<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Entradas */

/*
 * 
 *      Vista para generar entradas desde un evento
 * 
 */

// Recibe el nombre del evento del parametro del controlador
$nomevento = Yii::$app->getRequest()->getQueryParam('nomevento');
$this->title = 'Generar entrada de ' . $nomevento;

$this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['eventos/index']];
$this->params['breadcrumbs'][] = ['label' => $nomevento, 'url' => ['eventos/view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = ['label' => 'Entradas', 'url' => ['indexfromeventos', 'evento' => $model->evento, 'nomevento' => $nomevento]];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="entradas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formcrearentrada', [
        'model' => $model,
    ]) ?>

</div>
