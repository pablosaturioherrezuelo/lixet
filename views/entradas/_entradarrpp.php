<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
//$clave = $model->id;
/*
 * 
 *      Vista para mostrar una entrada asignada a un rrpp en un evento
 * 
 */

// Si tiene asignado un comprador el boton muestra la entrada en pdf, sino se permite cambiar de vendedor 
if($model->comprador){
    $boton = Html::a('Imprimir', ['/entradas/report','id' => $model->id], ['class' => 'btn btn-danger sombrabox']);
} else {
    $boton = Html::a('Cambiar vendedor', ['/entradas/cambiarvendedor','id' => $model->id, 'rrpp' => $model->vendedor, 'nomrrpp' => $model->vendedor0->nombre . " " . $model->vendedor0->apellidos], ['class' => 'btn btn-lxt sombrabox']);
}
?>
<!-- <?= var_dump($model) ?> -->
<div class="card sombrabox" style="margin-top: 10px; margin-bottom: 20px">
    <div class="card-body">
        
        <div class="caption text-center">
            <h4><?= $model->nomevento ?></h4>
            <h4><?= $model->tipo ?> <br> <?= $model->numero ?></h4>
            <?= Html::img('@web/img/eventos/'.$model->codigo.'.jpg', ['class' => 'imgview'])?>
            <h6><?= number_format($model->precio, 2, ',', '.') . "€" . $model->rrpp?></h6> <?=$model->rrpp?>
            <?= $boton ?>    
        </div>

    </div>
</div> 