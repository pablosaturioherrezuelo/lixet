<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Entradas */

/*
 * 
 *      Vista para cambiar el vendedor de una entrada
 * 
 */

$nomevento = $model->nomevento;
$evento = $model->evento;
// Recibe los parametros del controlador
$nomrrpp = Yii::$app->getRequest()->getQueryParam('nomrrpp');
$rrpp = Yii::$app->getRequest()->getQueryParam('rrpp');

$this->title = $model->evento0->nombre .' '. $model->evento0->edicion . " - ". $model->numero;

$this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['eventos/index']];
$this->params['breadcrumbs'][] = ['label' => $nomevento, 'url' => ['eventos/view', 'id' => $evento]];
$this->params['breadcrumbs'][] = ['label' => 'RRPPS', 'url' => ['rrpps/rrpps', 'evento' => $evento, 'nomevento' => $nomevento]];
$this->params['breadcrumbs'][] = ['label' => $nomrrpp, 'url' => ['entradas/entradasrrpp', 'rrpp' => $rrpp, 'nomrrpp' => $nomrrpp, 'evento' => $evento, 'nomevento' => $nomevento]];

$this->params['breadcrumbs'][] = "Cambiar vendedor de entrada: " . $this->title;
?>
<div class="entradas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formcambiarvendedor', [
        'model' => $model,
    ]) ?>

</div>
