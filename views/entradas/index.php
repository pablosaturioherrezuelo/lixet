<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

/*
 * 
 *  Index de las entradas 
 * 
 */

$this->title = 'Entradas';
$this->params['breadcrumbs'][] = $this->title;

// Se obtiene el rol del usuario registrado para mostrarle sus acciones permitidas
$rol = isset(array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]) ? array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]:'';
$permiso=0;
$crear = "";
$actioncolumn = [
                'header' => '',
                'content' => function($model) {
                    return Html::a('Mis Entradas',['misentradas'], ['class' => 'btn btn-lxt']);
                }  
            ];
switch ($rol) {
    case "Admin":
        // Acciones totales
        $permiso = 4;
        $crear = Html::a('Crear entradas', ['create'], ['class' => 'btn btn-lxt']);
        $actioncolumn = [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ];
        break;
    case "Jefe":
        $permiso = 3;
        // Solo puede ver sus entradas, imprimir una vendida o vender
        $crear = Html::a('Mis entradas', ['entradas/misentradas'], ['class' => 'btn btn-lxt']);
        $actioncolumn = [
                'header' => '',
                'content' => function($model) {
                    if($model->nomcomprador){
                        return Html::a('Imprimir',['report','id'=>$model->id], ['class' => 'btn btn-danger']);
                    } else {
                        return Html::a('Vender',['venderentrada','id'=>$model->id], ['class' => 'btn btn-lxt']);
                    }
                }  
            ];
        break;
    case "RRPP":
        $permiso = 2;
        break;
    case "Cliente":
        $permiso = 1;
        break;
}
?>
<div class="entradas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= $crear ?>
    </p>


    <?= GridView::widget([
        
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-hover table-responsive-md',],
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            'nomevento',
            'numentrada',
            //'numero',
            [
                'label' => 'Precio',
                'attribute' =>'precio',
                'contentOptions' => ['class' => 'text-center'],
                'value'=>function ($model) {
                    return number_format($model->precio, 2, ',', '.') . "€";
                },
            ],
            'tipo',
            'nomvendedor',
            'nomcomprador',
            'fecha',
            $actioncolumn,
        ],
    ]); ?>


</div>
