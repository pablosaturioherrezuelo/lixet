<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\assets\AppAsset;
use app\widgets\Alert;
AppAsset::register($this);

/*
 * 
 *      Vista que muestra en formato pdf una entrada
 * 
 */

// Busca la imagen que pertenece al evento y en su defecto muestra una estandar
if(file_exists(getcwd().'/img/eventos/'.$model->evento0->codigo.'.jpg')){
    $img = '@web/img/eventos/' . $model->evento0->codigo .'.jpg';
} else if(file_exists(getcwd().'/img/eventos/'.$model->evento0->codigo.'.png')){
    $img = '@web/img/eventos/' . $model->evento0->codigo .'.png';
} else {
    $img =  '@web/img/eventos/default.jpg';
}

?>

<div class="card text-white" >
    <?= Html::img($img, ['alt' => 'Imagen no encontrada', 'class' => 'card-img'])?>
    <div class="text-center t-bgdark" style="padding-left: 30px">
        <h4 class=""><?= $model->evento0->nombre ." - " . $model->evento0->edicion ."<br>". $model->numero . " - " . $model->tipo . "<br>" . number_format($model->precio, 2, ',', '.') . " € <br>" . $model->nomcomprador?> </h4>
        <h5><?=$ubicacion?></h5>
    </div>
</div>