<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

/*
 *
 *      Vista para mostrar los mejores vendedores de la plataforma
 * 
 */

$this->title = 'Mejores Vendedores';
$this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['eventos/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entradas-index">

    <h1><?= Html::encode($this->title) ?></h1>

   <?= GridView::widget([
        
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-hover table-responsive-md',],
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nomvendedor',
            'cantidad',
            //'comision',
            //'precio',
            //'beneficio',
            [
                'label' => 'Comisión',
                'attribute' =>'comision',
                'contentOptions' => ['class' => 'text-left'],
                'value'=>function ($model) {
                    return number_format($model->comision, 2, ',', '.') . "€";
                },
            ],
            [
                'label' => 'Precio',
                'attribute' =>'precio',
                'contentOptions' => ['class' => 'text-left'],
                'value'=>function ($model) {
                    return number_format($model->precio, 2, ',', '.') . "€";
                },
            ],
            [
                'label' => 'Beneficio',
                'attribute' =>'beneficio',
                'contentOptions' => ['class' => 'text-left'],
                'value'=>function ($model) {
                    return number_format($model->beneficio, 2, ',', '.') . "€";
                },
            ],
        ],
    ]); ?>


</div>
