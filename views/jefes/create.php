<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Jefes */

/*
 * 
 * Vista para asignar un rrpp a un grupo
 * 
 */

// Obtiene como parametro el jefe del grupo
$nomjefe = Yii::$app->getRequest()->getQueryParam('nomjefe');
$jefe = Yii::$app->getRequest()->getQueryParam('jefe');

$this->params['breadcrumbs'][] = ['label' => 'Grupos', 'url' => ['grupo']];

// Si el parametro del nombre esta vacio muestra enlace para volver al grupo del usuario, en su defecto muestra el grupo del jefe
if(isset($nomjefe)){
    $this->title = 'Asignar RRPP a ' . $nomjefe;
    $this->params['breadcrumbs'][] = ['label' => 'Grupo de ' . $nomjefe, 'url' => ['index2', 'jefe' => $jefe, 'nomjefe' => $nomjefe]];
} else {
    $this->title = 'Asignar RRPP';
    $this->params['breadcrumbs'][] = ['label' => 'Mi Grupo', 'url' => ['migrupo']];
}

$this->params['breadcrumbs'][] = $this->title;

?>
<div class="jefes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
