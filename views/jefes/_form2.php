<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Jefes */
/* @var $form yii\widgets\ActiveForm */

//$model->jefe = -1

/*
 * 
 *      Formulario para crear un nuevo jefe de grupo
 * 
 */

?>

<div class="jefes-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="d-none">
        <?= $form->field($model, 'jefe')->dropDownList($model->getdropdownJefes(),['prompt' => '','disabled' => 'disabled']) ?>
    </div>  
    <?= $form->field($model,'vendedor')->dropDownList($model->getdropdownRrpps(),['prompt' => '']) ?>
    
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-lxt']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
