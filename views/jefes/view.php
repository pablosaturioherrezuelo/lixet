<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Jefes */

/*
 * 
 *      Vista detallada de un vendedor y su jefe
 * 
 */

$this->title = $model->nomjefe == $model->nomrrpp ? 'Jefe: ' . $model->nomjefe : $model->nomjefe . ' - ' . $model->nomrrpp;
$this->params['breadcrumbs'][] = ['label' => 'Grupos', 'url' => ['grupo']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="jefes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-lxt']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas seguro de querer borrar este registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-hover table-responsive-md'],
        'attributes' => [
            //'id',
            'nomjefe',
            'nomrrpp',
        ],
    ]) ?>

</div>
