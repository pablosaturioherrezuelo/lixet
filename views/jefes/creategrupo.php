<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Jefes */

/*
 * 
 * Vista para crear un nuevo jefe de grupo
 * 
 */

$this->title = 'Asignar jefe de grupo';
$this->params['breadcrumbs'][] = ['label' => 'Grupos', 'url' => ['grupo']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jefes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form2', [
        'model' => $model,
    ]) ?>

</div>
