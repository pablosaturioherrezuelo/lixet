<?php
use yii\helpers\Html;
use yii\helpers\HTMLPurifier;
use circulon\widgets\ColumnListView;

/*
 * 
 *      Vista que muestra todos los jefes y la cantidad de vendedores a su cargo
 * 
 */
$titulo = "Grupos";
$this->title = $titulo;
$this->params['breadcrumbs'][] = $this->title;

// Se obtiene el rol del usuario registrado para mostrarle sus acciones permitidas
$rol = isset(array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]) ? array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]:'';
if($rol == "Admin"){
    // Si es administrador se le permite ver el grupo de jefes y crear un jefe nuevo
    $boton = Html::a('Crear jefe', ['/jefes/creategrupo',], ['class' => 'btn btn-lxt sombrabox']);
    $boton2 = Html::a('Jefes', ['/jefes/jefes',], ['class' => 'btn btn-lxt sombrabox']);
} else {
    $boton = "";
    $boton2 = "";
}

?>

<div class="well well-sm text-center">
    <h1 style="">
        <?=$titulo?>
    </h1>
</div>

<p>
    <?= $boton ?> <?= $boton2 ?>
</p>

<div class="">
    <?= ColumnListView::widget([ 
        'dataProvider' => $dataProvider,
        'itemView' => '_grupo',
        'layout'=> "\n{pager}\n{items}",
        'columns' => 2,
    ]);
    ?>
</div>