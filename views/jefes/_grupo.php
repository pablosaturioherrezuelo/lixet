<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
//$clave = $model->id;

/*
 * 
 * Vista que permite ver un jefe de grupo y la cantidad de vendedores a su cargo
 * 
 */
?>

<div class="card sombrabox" style="margin-top: 10px; margin-bottom: 20px">
    <div class="card-body">
        
        <div class="caption text-center">
            <h4><?= $model->nomjefe ?></h4>
            <h6><?= $model->integrantes ?> integrantes</h6>
            <?= Html::a('Administrar', ['/jefes/index2','jefe'=>$model->jefe, 'nomjefe' => $model->nomjefe], ['class' => 'btn btn-lxt sombrabox']) ?>
        </div>

    </div>
</div> 