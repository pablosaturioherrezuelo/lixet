<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


/*
 * 
 *      Vista que muestra todos los jefes de grupo
 * 
 */

$key = Yii::$app->getRequest()->getQueryParam('jefe');
$this->title = 'Jefes';

$this->params['breadcrumbs'][] = ['label' => 'Grupos', 'url' => ['jefes/grupo']];
$this->params['breadcrumbs'][] = $this->title;

// Se obtiene el rol del usuario registrado para mostrarle sus acciones permitidas
$rol = isset(array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]) ? array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]:'';
if($rol == "Admin"){
    // Si es admin se le permite crear un jefe
    $boton = Html::a('Crear jefe', ['/jefes/creategrupo',], ['class' => 'btn btn-lxt sombrabox']);
} else {
    $boton = "";
}

?>
<div class="jefes-index">
    
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= $boton ?> 
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-hover table-responsive-md'],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'jefe',
            //'vendedor',
            'nomjefe',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
