<?php
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

/*
 * 
 *  Vista que muestra los integrantes del grupo al que pertenece el usuario registrado
 * 
 */

$titulo = 'Mi Grupo';
$this->title = $titulo;
$this->params['breadcrumbs'][] = $this->title;
//$nombre = Yii::$app->user->identity->idrrpp0->nombre . " " . Yii::$app->user->identity->idrrpp0->apellidos;

// Obtiene el nombre y apellidos para mostrarlo
$nombre = isset(Yii::$app->user->identity->idrrpp0->nombre) ? Yii::$app->user->identity->idrrpp0->nombre:"";
$nombre = isset(Yii::$app->user->identity->idrrpp0->apellidos) ?  $nombre . " " . Yii::$app->user->identity->idrrpp0->apellidos:$nombre;

// Se obtiene el rol del usuario registrado para mostrarle sus acciones permitidas
$rol = isset(array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]) ? array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]:'';
$idrrpp = Yii::$app->user->identity->idrrpp;

if($rol == "Admin" or $rol == "Jefe"){
    // Si es administrador o jefe se le permite crear, ver, actualizar y borrar
    $boton = Html::a('Agregar RRPP', ['/jefes/create','jefe'=>$idrrpp,'nomjefe'=>$nombre], ['class' => 'btn btn-lxt sombrabox']);
    $actioncolumn = [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ];
} else {
    $boton = "";
    $actioncolumn = [];
}

?>
<div class="well well-sm text-center">
    <h1 style="">
        <?= $titulo ?>
    </h1>
</div>

<p>
    <?= $boton ?>
</p>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-hover table-responsive-md'],
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            'tipo',
            'nomrrpp',
            'telefono',
        ],
    ]); ?>