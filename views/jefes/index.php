<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


/*
 * 
 * Vista principal de todos los vendedores que esten en grupos y sus jefes
 * 
 */

$key = Yii::$app->getRequest()->getQueryParam('jefe');
$this->title = 'Grupo';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jefes-index">
    
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Agregar RRPP', ['create','jefe' => $key], ['class' => 'btn btn-lxt']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-hover table-responsive-md'],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'jefe',
            //'vendedor',
            'nomjefe',
            'nomrrpp',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
