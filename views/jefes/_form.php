<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Jefes */
/* @var $form yii\widgets\ActiveForm */

/*
 * 
 * Formulario para asignar un RRPP a un grupo
 * 
 */
?>

<div class="jefes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'jefe')->dropDownList($model->getdropdownJefes(),['disabled' => false]) ?>

    <?= $form->field($model, 'vendedor')->dropDownList($model->getdropdownRrpps()) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-lxt']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>
