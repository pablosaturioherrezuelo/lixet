<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);

// Se obtiene el rol del usuario registrado para mostrarle sus acciones permitidas
$rol = isset(array_keys(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId()))[0]) ? array_keys(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId()))[0]:'';
$permiso=0;
$manual = "";
switch ($rol) {
    case "Admin":
        $permiso = 4;
        $manual = Html::a('Manual de Administrador', Yii::$app->request->baseUrl .'/manuals/' . 'Manual-Administrador-LIXET.pdf', ['class' => '', 'target' => '_blank']);
        break;
    case "Jefe":
        $permiso = 3;
        $manual = Html::a('Manual de Jefe de RRPP', Yii::$app->request->baseUrl .'/manuals/' . 'Manual-Jefe-RRPP-LIXET.pdf', ['class' => '', 'target' => '_blank']);
        break;
    case "RRPP":
        $permiso = 2;
        $manual = Html::a('Manual de RRPP', Yii::$app->request->baseUrl .'/manuals/' . 'Manual-RRPP-LIXET.pdf', ['class' => '', 'target' => '_blank']);
        break;
    case "Cliente":
        $permiso = 1;
        break;
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/img/logos/lixet-icon.ico" type="image/x-icon" />
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>
<header>
    <?php
    NavBar::begin([
        //'brandLabel' => Yii::$app->name,
        'brandLabel' => Html::img('@web/img/logos/lixet logo-rectangle.png',[ 'style' => 'height: 20px'],
                    ['alt' => 'Imagen no encontrada'], ['class' => 'navbar-brand']),
        'brandUrl' => $urllogo = !Yii::$app->user->isGuest ? $permiso != 0 ? Yii::$app->homeUrl : 'sinacciones' : 'login',
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
        ],
    ]);
    
    $items = [
        //['label' => 'Eventos', 'url' => ['/eventos/index']],
        //['label' => 'Grupos', 'url' => ['/jefes/grupo']],
    ];
    
    if (Yii::$app->user->isGuest) {
        //$items[] = ['label' => 'Contacto', 'url' => ['/site/contact']];
        //$items[] = ['label' => 'Registro', 'url' => ['/user/registration']];
        $items[] = ['label' => 'Iniciar sesión', 'url' => ['/site/login']];
    } else {
        //$items[] = ['label' => 'Manage', 'url' => ['/site/manage']];
        switch ($permiso) {
            case 4:
                // Para los administradores
                $items[] = 
                    ['label' => 'Usuarios', 'url' => ['/user/index'], 'items' => [
                        ['label' => 'Usuarios', 'url' => ['/user/index']],
                        ['label' => 'Asignaciones', 'url' => ['/rbac/assignment']],
                        ['label' => 'Roles', 'url' => ['/rbac/role']],
                        ['label' => 'Permisos', 'url' => ['/rbac/permission']],
                        ['label' => 'Rutas', 'url' => ['/rbac/route']],
                    ]
                ];
            case 3:
                // Administradores y jefes
                if($permiso>=4){
                    $items[] = 
                        ['label' => 'Eventos', 'url' => ['/eventos/index'], 'items' => [
                            ['label' => 'Eventos', 'url' => ['/eventos/index']],
                            ['label' => 'Recintos', 'url' => ['/recintos/index']],
                            ['label' => 'Alquileres', 'url' => ['/alquileres/index']],
                        ]
                    ];
                } else {
                    $items[] = ['label' => 'Eventos', 'url' => ['/eventos/index']];
                }
                $items[] = ['label' => 'Entradas', 'url' => ['/entradas/index']];
                $items[] = ['label' => 'Grupos', 'url' => ['/jefes/grupo']];
                $items[] = ['label' => 'RRPPS', 'url' => ['/rrpps/index']];
                if($permiso>=4){
                    // Solo administradores
                    $items[] = ['label' => 'Clientes', 'url' => ['/clientes/index']];
                } else {
                   $items[] = ['label' => 'Clientes', 'url' => ['/clientes/misclientes']]; 
                }
            case 2:
                 if($permiso==2){
                    // Solo para vendedores
                    $items[] = ['label' => 'Todos los Eventos', 'url' => ['/eventos/index']];
                    $items[] = ['label' => 'Entradas', 'url' => ['/entradas/misentradas']];
                    $items[] = ['label' => 'Grupos', 'url' => ['/jefes/migrupo']];
                    $items[] = ['label' => 'Clientes', 'url' => ['/clientes/misclientes']];
                }
            case 1:
                if($permiso==1){
                    // Solo para clientes
                    $items[] = ['label' => 'Eventos', 'url' => ['/eventos/index']];
                    $items[] = ['label' => 'Entradas', 'url' => ['/entradas/misentradas']];
                }
                //$items[] = ['label' => 'Contacto', 'url' => ['/site/contact']];        
        }
        
        if($permiso == 0){
            // Guia para los usuarios que no tienen rol asignado
            $items[] = ['label' => '¿Por que no veo las acciones básicas?', 'url' => ['/site/sinacciones']];
        }
        
        $items[] = '<li>'
        . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
        . Html::submitButton(
            'Cerrar sesión (' . Yii::$app->user->identity->username . ')',
            ['class' => 'btn btn-link logout']
        )
        . Html::endForm()
    . '</li>';
    }
    
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => $items
    ]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-right">&copy; Lixet <?= date('Y') ?></p>
        <p class="float-left"><?= /*Yii::powered()*/"" ?>
        <?= $manual ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
