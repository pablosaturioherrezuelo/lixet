<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/*
 * VISTA DE LOS CLIENTES QUE HAYAN COMPRADO ENTRADAS AL USUARIO REGISTRADO
 * Solo se permite ver, crear y actualizar desde esta vista ya que es de las acciones basicas
 */

$this->title = 'Mis Clientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clientes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear cliente', ['createfromindex'], ['class' => 'btn btn-lxt']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-hover table-responsive-md'],
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'id',
            'email:email',
            'nombre',
            'apellidos',
            'telefono',
            'f_nacimiento',
            [
                'header' => '',
                'content' => function($model) {
                    return Html::a('Actualizar',['updatefromindex','id'=>$model->id], ['class' => 'btn btn-lxt']);
                }  
            ],
        ],
    ]); ?>


</div>
