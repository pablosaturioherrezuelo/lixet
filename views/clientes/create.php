<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Clientes */

$this->title = 'Crear nuevo cliente';


/*
 * Se obtiene el rol del usuario registrado para mostrarle sus respectivas migas de pan
 */

$rol = isset(array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]) ? array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]:'';
if($rol == "Admin"){
    $this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
} else {
    $this->params['breadcrumbs'][] = ['label' => 'Mis Clientes', 'url' => ['misclientes']];
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clientes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
