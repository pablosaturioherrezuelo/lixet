<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use borales\extensions\phoneInput\PhoneInput;

/* @var $this yii\web\View */
/* @var $model app\models\Clientes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clientes-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?> 
            <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>
        </div>
        
        <div class="col-md-4">
            <!-- Por defecto el codigo de telefono de españa -->
            <?=$form->field($model, 'telefono')->widget(PhoneInput::className(), [
                'jsOptions' => ['preferredCountries' => ['es']]])?>
            <?= $form->field($model, 'f_nacimiento')->input('date')?>
        </div>
        
        <div class="col-md-12">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
    
    </div>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-lxt']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
