<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clientes';

/*
 * Se obtiene el rol del usuario registrado para mostrarle sus respectivas funciones permitidas 
 */

$rol = isset(array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]) ? array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]:'';
$boton = "";
$actioncolumn = [
                'header' => '',
                'content' => function($model) {
                    return "";
                }  
            ];
            
if($rol == "Admin"){
    $boton = Html::a('Crear cliente', ['create'], ['class' => 'btn btn-lxt']);
    $actioncolumn = [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ];
} else if($rol == "Jefe"){
    // Solo se permite actualizar y crear clientes a los jefes
    $boton = Html::a('Crear cliente', ['create'], ['class' => 'btn btn-lxt']);
    $actioncolumn = [
                'header' => '',
                'content' => function($model) {
                    return Html::a('Actualizar',['/update','id'=>$model->id,], ['class' => 'btn btn-lxt']);
                }  
            ];
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clientes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= $boton ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-hover table-responsive-md'],
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'id',
            'nombre',
            'apellidos',
            'f_nacimiento',
            'email:email',
            'telefono', 
            $actioncolumn,
        ],
    ]); ?>


</div>
