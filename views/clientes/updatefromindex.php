<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Clientes */

/*
 * 
 *      Vista para actualizar clientes desde la vista Mis Clientes
 * 
 */

$this->title = 'Actualizar cliente: ' . $model->nombre . ' ' . $model->apellidos;
$this->params['breadcrumbs'][] = ['label' => 'Mis Clientes', 'url' => ['misclientes']];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="clientes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
