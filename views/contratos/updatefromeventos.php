<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contratos */

/*
 * 
 *      Vista para actualizar un contrato desde un evento
 * 
 */

$union = $model->nomevento . ' - ' . $model->nomrrpp;
$this->title = 'Cambiar RRPP de evento: ' . $union;
$from = Yii::$app->getRequest()->getQueryParam('from');
if($from === 'rrpps'){
    $this->params['breadcrumbs'][] = ['label' => 'RRPPS', 'url' => ['rrpps/index']];
    $this->params['breadcrumbs'][] = ['label' => 'Contratos: ' . $model->nomrrpp, 'url' => ['contratos', 'rrpp' => $model->rrpp,'nomrrpp' => $model->nomrrpp, 'from'=>$from]];
    $this->params['breadcrumbs'][] = ['label' => $model->nomevento, 'url' => ['viewfromevento', 'id' => $model->id,'from'=>$from]];
} else if($from === null){
    $this->params['breadcrumbs'][] = ['label' => 'Contratos', 'url' => ['contratos', 'rrpp' => $model->rrpp, 'nomrrpp' => $model->nomrrpp]];
    $this->params['breadcrumbs'][] = ['label' => $union, 'url' => ['viewfromevento', 'id' => $model->id]];
} else {
    $this->params['breadcrumbs'][] = ['label' => 'Contratos', 'url' => ['contratos', 'rrpp' => $model->rrpp, 'nomrrpp' => $model->nomrrpp]];
    $this->params['breadcrumbs'][] = ['label' => $union, 'url' => ['viewfromevento', 'id' => $model->id]];
}
$this->params['breadcrumbs'][] = 'Cambiar';
?>
<div class="contratos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
