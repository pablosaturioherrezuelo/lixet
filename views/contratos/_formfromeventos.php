<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contratos */
/* @var $form yii\widgets\ActiveForm */

/*
 * 
 *  Formulario para generar un contrato desde un evento
 * 
 */

// Obtener el evento y su nombre desde el parametro del controlador
$nomevento = Yii::$app->getRequest()->getQueryParam('nomevento');
$evento = Yii::$app->getRequest()->getQueryParam('evento');
?>

<div class="contratos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'evento')->dropDownList($model->getdropdownEventos()) ?>
    
    <?= $form->field($model, 'rrpp')->dropDownList($model->getdropdownRrpps()) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-lxt']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
