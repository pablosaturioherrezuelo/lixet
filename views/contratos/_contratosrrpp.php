<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
$from = Yii::$app->getRequest()->getQueryParam('from');
/*
 * 
 *      Vista que muestra un evento en el que esta contratado un rrpp
 * 
 */


// Se busca la imagen respectiva y sino se muestra una por defecto
$img = "";
if(file_exists(getcwd().'/img/eventos/'.$model->codigo.'.jpg')){
    $img = Html::img('@web/img/eventos/'.$model->codigo.'.jpg', ['class' => 'imgview']);
} else if(file_exists(getcwd().'/img/eventos/'.$model->codigo.'.png')){
    $img = Html::img('@web/img/eventos/'.$model->codigo.'.png', ['class' => 'imgview']);
} else {
    $img = Html::img('@web/img/eventos/default.jpg', ['class' => 'imgview']);
}
?>
<!-- <?= var_dump($model) ?> -->
<div class="card sombrabox" style="margin-top: 10px; margin-bottom: 20px">
    <div class="card-body">
        <div class="caption text-center">
            <h4><?= $model->nomevento ?></h4>
            <?= $img?>
            <?= Html::a('Ver', ['/contratos/viewfromevento', 'id' => $model->id, 'from'=>$from], ['class' => 'btn btn-lxt sombrabox']) ?>
        </div>

    </div>
</div> 