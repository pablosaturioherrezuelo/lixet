<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contratos */

/*
 * 
 *      Vista para crear un contratos de un de un evento recibido como parametro
 * 
 */

// Obtener el id evento, nombre del evento desde el parametro del controlador
$nomevento = Yii::$app->getRequest()->getQueryParam('nomevento');
$evento = Yii::$app->getRequest()->getQueryParam('evento');
$this->title = 'Asignar RRPP a ' . $nomevento;
$this->params['breadcrumbs'][] = ['label' => $nomevento, 'url' => ['rrpps/rrpps', 'evento' => $evento, 'nomevento' => $nomevento]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contratos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formfromeventos', [
        'model' => $model,
    ]) ?>

</div>
