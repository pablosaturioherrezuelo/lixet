<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contratos */

$this->title = 'Asignar RRPP a evento';

$from = Yii::$app->getRequest()->getQueryParam('from');
if($from === 'rrpps'){
    $this->params['breadcrumbs'][] = ['label' => 'RRPPS', 'url' => ['rrpps/index']];
    $this->params['breadcrumbs'][] = ['label' => 'Contratos: ' . $model->rrpp0->nombre . ' ' . $model->rrpp0->apellidos, 'url' => ['contratos', 'rrpp' => $model->rrpp,'nomrrpp' => $model->rrpp0->nombre . ' ' . $model->rrpp0->apellidos, 'from'=>$from]];
    $this->params['breadcrumbs'][] = $this->title;
} else {
    $this->params['breadcrumbs'][] = ['label' => 'Contratos', 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
}
?>
<div class="contratos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
