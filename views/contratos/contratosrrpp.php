<?php
use yii\helpers\Html;
use yii\helpers\HTMLPurifier;
use circulon\widgets\ColumnListView;

/*
 * 
 *      Vista que muestra todos los contratos de un RRPP
 * 
 */

// Obtener el id evento, nombre del evento, id de rrpp y nombre de rrpp desde el parametro del controlador
$nomrrpp = Yii::$app->getRequest()->getQueryParam('nomrrpp');
$idrrpp = Yii::$app->getRequest()->getQueryParam('rrpp');
$nomevento = Yii::$app->getRequest()->getQueryParam('nomevento');
$evento = Yii::$app->getRequest()->getQueryParam('evento');
$titulo = "Eventos en los que está activo " . $nomrrpp;
$this->title = $titulo;
$boton ="";

// Migas de pan acordes a la navegacion del usuario
if(Yii::$app->getRequest()->getQueryParam('from') === null){
    $this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['eventos/index']];
    $this->params['breadcrumbs'][] = ['label' => $nomevento, 'url' => ['eventos/view', 'id' => $evento]];
    $this->params['breadcrumbs'][] = ['label' => 'RRPPS', 'url' => ['rrpps/rrpps', 'evento' => $evento, 'nomevento' => $nomevento]];
    $this->params['breadcrumbs'][] = 'Contratos: ' . $nomrrpp;
    $boton=Html::a('Añadir a evento', ['/contratos/rrppevento', 'rrpp' => $idrrpp], ['class' => 'btn btn-lxt sombrabox']);
} else if(Yii::$app->getRequest()->getQueryParam('from') == "rrpps") {
    $this->params['breadcrumbs'][] = ['label' => 'RRPPS', 'url' => ['rrpps/index']];
    $this->params['breadcrumbs'][] = 'Contratos: ' . $nomrrpp;
    $boton = Html::a('Añadir a evento', ['/contratos/rrppevento', 'rrpp' => $idrrpp, 'from' => 'rrpps'], ['class' => 'btn btn-lxt sombrabox']);
}else {
    $this->params['breadcrumbs'][] = ['label' => 'Contratos', 'url' => ['contratos/index']];
    $this->params['breadcrumbs'][] = 'Contratos: ' . $nomrrpp;
    $boton = "";
}

?>

<div class="well well-sm text-center">
    <h1 style="">
        <?=$titulo?>
    </h1>
</div>

<p> <!-- Crea un contrato con el rrpp que se este mostrando -->
    <?= $boton ?>
</p>

<div class="">
    <?= ColumnListView::widget([ 
        'dataProvider' => $dataProvider,
        'itemView' => '_contratosrrpp',
        'layout'=> "\n{pager}\n{items}",
        'columns' => 2,
    ]);
    ?>
</div>