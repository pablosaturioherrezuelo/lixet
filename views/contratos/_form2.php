<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contratos */
/* @var $form yii\widgets\ActiveForm */

/*
 * 
 *  No usar, usado para pruebas
 *  
 */

?>

<div class="contratos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'evento')->dropDownList($model->getdropdownEventos()) ?>
    <!--<?= $form->field($model, 'rrpp')->dropDownList($model->getdropdownRrpps()) ?>-->   
    
    <?= $form->field($model, 'rrpp')->checkboxList($model->getdropdownRrpps()) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-lxt']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
