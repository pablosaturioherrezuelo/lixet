<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Contratos */

/*
 * 
 *      Vista detallada de un contrato desde un evento
 * 
 */
 $this->title = 'Contrato: ' . $model->nomevento . ' (' . $model->nomrrpp . ')';
$from = Yii::$app->getRequest()->getQueryParam('from');
$borrar =Html::a('Borrar', ['deletefromeventos', 'id' => $model->id, 'evento' => $model->evento, 'nomevento' => $model->nomevento], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas seguro de querer borrar este registro?',
                'method' => 'post',
            ],
        ]);
if($from === 'rrpps'){
    $this->params['breadcrumbs'][] = ['label' => 'RRPPS', 'url' => ['rrpps/index']];
    $this->params['breadcrumbs'][] = ['label' => 'Contratos: ' . $model->nomrrpp, 'url' => ['contratos', 'rrpp' => $model->rrpp,'nomrrpp' => $model->nomrrpp, 'from'=>$from]];
    $this->params['breadcrumbs'][] = $this->title;
    $borrar =Html::a('Borrar', ['deletefromrrpps', 'id'=>$model->id, 'rrpp' => $model->rrpp, 'nomrrpp' => $model->nomrrpp], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas seguro de querer borrar este registro?',
                'method' => 'post',
            ],
        ]);
} else if($from === null){
    $this->params['breadcrumbs'][] = ['label' => 'Contratos', 'url' => ['contratos', 'rrpp' => $model->rrpp, 'nomrrpp' => $model->nomrrpp,'evento'=>$model->evento,'nomevento'=>$model->nomevento]];
    $this->params['breadcrumbs'][] = $this->title;
} else {
    $this->params['breadcrumbs'][] = ['label' => 'Contratos', 'url' => ['contratos', 'rrpp' => $model->rrpp, 'nomrrpp' => $model->nomrrpp,'evento'=>$model->evento,'nomevento'=>$model->nomevento]];
    $this->params['breadcrumbs'][] = $this->title;
}

\yii\web\YiiAsset::register($this);
?>
<div class="contratos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['updatefromeventos', 'id' => $model->id,'from'=>$from], ['class' => 'btn btn-lxt']) ?>
        <?= $borrar ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-hover table-responsive-md'],
        'attributes' => [
            //'id',
            //'evento',
            //'rrpp',
            'nomevento',
            'nomrrpp',
        ],
    ]) ?>

</div>
