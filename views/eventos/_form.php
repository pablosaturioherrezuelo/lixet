<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Eventos */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="eventos-form">

    <?php $form = ActiveForm::begin(/*[
        "method" => "post",
        "enableClientValidation" => true,
        "options" => ["enctype" => "multipart/form-data"],]*/)
    ?>
    
    <div class="row">
        
        <div class="col-md-12">
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
        </div>
        
        <div class="col-md-3">
            <?= $form->field($model, 'codigo')->textInput(['maxlength' => true]) ?>
        </div>
        
        <div class="col-md-3">
            <?= $form->field($model, 'edicion')->textInput() ?>
        </div>
        
        <div class="col-md-6">
            <?= $form->field($model, 'tipo')->textInput(['maxlength' => true]) ?>
        </div>
 
        <div class="col-md-6">
            <?= $form->field($model, "file[]")->fileInput(['multiple' => true]) ?>
        </div>
        
        <div class="col-md-6">
            <?= $form->field($model, "filemain[]")->fileInput(['multiple' => true]) ?>
        </div>
        
    </div>
    
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-lxt']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>
    
</div>
