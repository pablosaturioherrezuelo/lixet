<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Eventos */

$this->title = 'Actualizar evento: ' . $model->nombre . ' (' . $model->codigo . '-' . $model->edicion . ')';
$this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['eventos/index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['eventos/view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="eventos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
