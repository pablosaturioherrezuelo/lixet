<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Eventos */

/*
 * 
 *      Vista detallada de un evento y sus acciones de gestion
 * 
 */

$this->title = 'Evento: ' . $model->nombre . ' (' . $model->codigo . '-' . $model->edicion . ')';
$this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$ocultargrupo1 = "d-none";
$ocultarranking = "d-none";
$linkentradas = "/entradas/misentradaseventos";

// Se obtiene el rol del usuario registrado para mostrarle sus acciones permitidas
$rol = isset(array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]) ? array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]:'';
$permiso=0;
switch ($rol) {
    case "Admin":
        // Permisos totales
        $permiso = 4;
        $ocultargrupo1 = "";
        $linkentradas = '/entradas/indexfromeventos';
        $ocultarranking = "";
        break;
    case "Jefe":
        // Puede ver todas las entradas de ese evento
        $permiso = 3;
        $linkentradas = '/entradas/indexfromeventos';
        $ocultarranking = "";
        break;
    case "RRPP":
        $permiso = 2;
        break;
    case "Cliente":
        $permiso = 1;
        break;
}

?>
<div class="eventos-view">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <!-- Se busca la imagen asignada al evento y en su defecto se pone una estandar -->
    <?php
    if(file_exists(getcwd().'/img/eventos/'.$model->codigo.'.jpg')){
        echo Html::img('@web/img/eventos/'.$model->codigo.'.jpg', ['class' => 'imgview']);
    } else if(file_exists(getcwd().'/img/eventos/'.$model->codigo.'.png')){
        echo Html::img('@web/img/eventos/'.$model->codigo.'.png', ['class' => 'imgview']);
    } else {
        echo Html::img('@web/img/eventos/default.jpg', ['class' => 'imgview']);
    } ?>
    
    <p></p>
    
    <!-- Grupo de botones con las acciones de gestion -->
    <div class="btn-toolbar mb-3" role="toolbar" aria-label="Toolbar with button groups">
        <div class="btn-group mr-2 <?=$ocultargrupo1?>" role="group" aria-label="First group">
            <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-lxt'])?>
            <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => '¿Estas seguro de querer borrar este registro?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>
        <div class="btn-group mr-2 text-right" role="group" aria-label="First group">
            <?= Html::a('Entradas', [$linkentradas, 'evento' => $model->id, 'nomevento' => $this->title], ['class' => 'btn btn-dark']) ?>
            <?= Html::a('RRPPS', ['/rrpps/rrpps', 'evento' => $model->id, 'nomevento' => $model->nombre . ' - ' . $model->edicion ], ['class' => "btn btn-dark $ocultarranking"]) ?>
            <?= Html::a('Recintos', ['/alquileres/index2', 'evento' => $model->id, 'nomevento' => $model->nombre . ' (' . $model->codigo . '-' . $model->edicion . ')'], ['class' => "btn btn-dark  $ocultargrupo1"]) ?>
            <?= Html::a('Ranking', ['/entradas/mejoresvendedoresevento', 'evento' => $model->id, 'nomevento' => $model->nombre . ' ' . $model->edicion], ['class' => "btn btn-dark $ocultarranking"]) ?>
        </div>
    </div>
    

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-hover table-responsive-md'],
        'attributes' => [
            //'id',
            'nombre',
            'edicion',
            'codigo',
            'tipo',
        ],
    ]) ?>

</div>
