<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


/*
 * 
 *      Vista que muestra todos los eventos
 * 
 */

$this->title = 'Eventos';
$this->params['breadcrumbs'][] = $this->title;

// Se obtiene el rol del usuario registrado para mostrarle sus acciones permitidas
$rol = isset(array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]) ? array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]:'';
$permiso=0;
$ranking = "";
$crear = "";
// Por defecto solo pueden acceder a sus entradas de ese evento
$actioncolumn = [
                'header' => '',
                'content' => function($model) {
                    return Html::a('Entradas',['/entradas/misentradaseventos','evento'=>$model->id,'nomevento' => $model->nombre . ' ' . $model->edicion ], ['class' => 'btn btn-lxt']);
                }  
            ];
switch ($rol) {
    case "Admin":
        // Permisos totales para ver, crear y borrar y ademas ver el ranking de vendedores de ese evento
        $permiso = 4;
        $crear = Html::a('Crear evento', ['create'], ['class' => 'btn btn-lxt']);
        $ranking = Html::a('Mejores vendedores', ['entradas/mejoresvendedores'], ['class' => "btn btn-lxt float-right"]);
        $actioncolumn = [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ];
        break;
    case "Jefe":
        // Permisos para ver el detalle del evento y el ranking de vendedores de ese evento
        $permiso = 3;
        $ranking = Html::a('Mejores vendedores', ['entradas/mejoresvendedores'], ['class' => "btn btn-lxt float-right"]);
        $actioncolumn = [
                'header' => '',
                'content' => function($model) {
                    return Html::a('Ver detalle',['view','id'=>$model->id], ['class' => 'btn btn-lxt']);
                }  
            ];
        break;
    case "RRPP":
        $permiso = 2;
        break;
    case "Cliente":
        $permiso = 1;
        break;
}

?>
<div class="eventos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-md-12">
            <?= $crear ?>
            <?= $ranking ?>
        </div>
    </div>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-hover table-responsive-md'],
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'id',
            'nombre',
            'edicion',
            'codigo',
            'tipo',
            $actioncolumn,
        ],
    ]); ?>


</div>
