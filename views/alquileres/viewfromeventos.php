<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\Alquileres */

$t = $model->nomrecinto;

$nomevento = $model->evento0->nombre . ' (' . $model->evento0->codigo . '-' . $model->evento0->edicion . ')';
$this->title = 'Alquiler de ' . $nomevento . ' en ' . $t;

$this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['eventos/index']]; // Migas de pan a eventos
$this->params['breadcrumbs'][] = ['label' => $nomevento, 'url' => ['eventos/view', 'id' => $model->evento]];
$this->params['breadcrumbs'][] = ['label' => 'Alquileres de ' . $nomevento, 'url' => ['alquileres/index2', 'evento' => $model->evento, 'nomevento' => $nomevento]];
$this->params['breadcrumbs'][] = $t;
\yii\web\YiiAsset::register($this);
?>
<div class="alquileres-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['updatefromeventos', 'id' => $model->id], ['class' => 'btn btn-lxt']) ?>
        <?= Html::a('Borrar', ['deletefromeventos', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas seguro de querer borrar este registro?',
                'method' => 'post',
            ],
        ]) ?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-hover table-responsive-md'],
        'attributes' => [
            //'id',       
            'nomevento',
            'nomrecinto',
            //'evento',
            //'recinto',
            'f_inicio',
            'h_inicio',
            'f_final',
            'h_final',
            'telefono',
            'tipo',
            'ubicacion',
            'aforo',   
        ],
    ]) ?>
    
</div>
