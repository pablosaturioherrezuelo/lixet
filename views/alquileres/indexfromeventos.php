<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$evento = Yii::$app->getRequest()->getQueryParam('evento');
$nomevento = Yii::$app->getRequest()->getQueryParam('nomevento');
$this->title = 'Alquileres de ' . $nomevento;
$this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['eventos/index']]; // Migas de pan a eventos
$this->params['breadcrumbs'][] = ['label' => $nomevento, 'url' => ['eventos/view', 'id' => $evento]];
$this->params['breadcrumbs'][] = $this->title;

$actioncolumn = [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    if ($action == "view") {
                        return Url::to(['viewfromeventos', 'id' => $model->id]);
                    } else if ($action == "update"){
                         return Url::to(['updatefromeventos', 'id' => $model->id]);
                    } else if ($action == "delete"){
                         return Url::to(['deletefromeventos', 'id' => $model->id]);
                    }
                 }
            ];
?>
<div class="alquileres-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear alquiler', ['create2','evento'=>$evento], ['class' => 'btn btn-lxt']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-hover table-responsive-md'],
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'evento',
            //'recinto',       
            'nomevento',
            'nomrecinto',
            'f_inicio',
            //'h_inicio',
            'f_final',
            //'h_final',
            $actioncolumn,
        ],
    ]); ?>


</div>
