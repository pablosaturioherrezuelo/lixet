<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Alquileres */

$this->title = 'Actualizar Alquiler: ' . $model->evento0->nombre . ' ' . $model->evento0->edicion;
$this->params['breadcrumbs'][] = ['label' => 'Alquileres', 'url' => ['alquileres/index']];
$this->params['breadcrumbs'][] = ['label' => $model->evento0->nombre . ' ' . $model->evento0->edicion . ' en ' . $model->recinto0->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="alquileres-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
