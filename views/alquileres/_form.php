<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm

/* @var $this yii\web\View */
/* @var $model app\models\Alquileres */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alquileres-form">

    <?php $form = ActiveForm::begin([
            'id' => 'login-form']) ?>
    
    <div class="row">
        <!-- seleccion del desplegable en lugar de introducir ids -->
        <div class="col-md-6">
            <?= $form->field($model, 'evento')->dropDownList($model->getdropdownEventos()) ?>
            <!--<?= $form->field($model, 'evento')->textInput() ?>-->
        </div>
        <div class="col-md-6">
            <!--<?= $form->field($model, 'recinto')->textInput() ?>-->
             <?= $form->field($model, 'recinto')->dropDownList($model->getdropdownRecintos()) ?>
            
            <hr style="margin-bottom: 5px; width: 1px">
            
        </div>
    </div>
    
    
    
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'f_inicio')->input('date')?>
            <?= $form->field($model, 'h_inicio')->input('time')?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'f_final')->input('date')?>
            <?= $form->field($model, 'h_final')->input('time') ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-lxt']) ?>
    </div>
    
    
    <?php ActiveForm::end(); ?>

</div>
