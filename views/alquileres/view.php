<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\Alquileres */

$this->title = 'Alquiler: ' . $model->nomevento . ' (' . $model->nomrecinto . ')';
//$this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['eventos/index']];
$this->params['breadcrumbs'][] = ['label' => 'Alquileres', 'url' => ['alquileres/index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="alquileres-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-lxt']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas seguro de querer borrar este registro?',
                'method' => 'post',
            ],
        ]) ?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-hover table-responsive-md'],
        'attributes' => [
            //'id',       
            'nomevento',
            'nomrecinto',
            //'evento',
            //'recinto',
            'f_inicio',
            'h_inicio',
            'f_final',
            'h_final',
            'telefono',
            'tipo',
            'ubicacion',
            'aforo',   
        ],
    ]) ?>
    
</div>
