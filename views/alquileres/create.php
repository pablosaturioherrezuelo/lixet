<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Alquileres */

$this->title = 'Asignar ubicación';
$from = Yii::$app->getRequest()->getQueryParam('evento');
$crear = "";
if($from !== null){
    //$nomevento = Yii::$app->getRequest()->getQueryParam('nomevento') === null ? $model->evento0->nombre . ' ' . $model->evento0->edicion : Yii::$app->getRequest()->getQueryParam('nomevento');
    $nomevento = isset($model->evento0->nombre) ? $model->evento0->nombre . ' ' . $model->evento0->edicion : Yii::$app->getRequest()->getQueryParam('nomevento');
    $this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['eventos/index']];
    if($nomevento!== null){
        $this->params['breadcrumbs'][] = ['label' => $nomevento, 'url' => ['eventos/view', 'id' => $model->evento]];
        $crear = Html::a('Crear ubicación', ['/recintos/create2', 'evento' => isset($model->evento) ? $model->evento : 0, 'nomevento' => $nomevento], ['class' => 'btn btn-lxt']);
    }
    
}else {
    $this->params['breadcrumbs'][] = ['label' => 'Alquileres', 'url' => ['alquileres/index']];
    //$crear = Html::a('Crear ubicación', ['/recintos/create'], ['class' => 'btn btn-lxt']);
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alquileres-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <!-- Crea una ubicacion con el evento enviado como parametro -->
        <?= $crear ?>
    </p>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
