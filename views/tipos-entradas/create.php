<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TiposEntradas */

$this->title = 'Create Tipos Entradas';
$this->params['breadcrumbs'][] = ['label' => 'Tipos Entradas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipos-entradas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
