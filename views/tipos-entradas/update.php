<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TiposEntradas */

$this->title = 'Actualizar tipos de entradas: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tipos Entradas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tipos-entradas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
