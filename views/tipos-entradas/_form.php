<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TiposEntradas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tipos-entradas-form">

    <?php $form = ActiveForm::begin(); ?>

    <h1 style="color: red">HAY QUE PENSAR COMO HACER ESTO</h1>
    <?= $form->field($model, 'evento')->dropDownList($model->getdropdownEventos()) ?>
    <?= $form->field($model, 'numero')->dropDownList($model->getdropdownNumero()) ?>
    
    <?= $form->field($model, 'evento')->textInput() ?>

    <?= $form->field($model, 'numero')->textInput() ?>

    <?= $form->field($model, 'tipo')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-lxt']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
