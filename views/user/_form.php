<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

/*
 * 
 * Formulario para crear o actualizar un usuario
 * 
 */

?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        
        <div class="col-md-6">
            <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
        </div>
        
        <div class="col-md-6">
            <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
        </div>
        
        <div class="col-md-12">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        
        <div class="col-md-6">
            <?= $form->field($model, 'idrrpp')->dropDownList($model->getdropdownRrpps(),['prompt' => '']) ?>
        </div>
       
        <div class="col-md-6">
            <?= $form->field($model, 'idcliente')->dropDownList($model->getdropdownClientes(),['prompt' => '']) ?>
        </div>
        
    </div>
    
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-lxt']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
