<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Recintos';
$this->params['breadcrumbs'][] = $this->title;

/*
 * 
 * Vista para ver todos los recintos
 * 
 */
?>
<div class="recintos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear recinto', ['recintos/create'], ['class' => 'btn btn-lxt']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-hover table-responsive-md'],
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'id',
            'nombre',
            'ubicacion',
            'aforo',
            'tipo',
            'telefono',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
