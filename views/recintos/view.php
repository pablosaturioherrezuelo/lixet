<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Recintos */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Recintos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

/*
 * 
 * Vista detallada de un recinto
 * 
 */

?>
<div class="recintos-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-lxt']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Esta seguro de querer borrar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-hover table-responsive-md'],
        'attributes' => [
            //'id',
            'nombre',
            'ubicacion',
            'aforo',
            'tipo',
            'telefono',
        ],
    ]) ?>

</div>
