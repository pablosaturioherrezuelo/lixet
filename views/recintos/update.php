<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recintos */

$this->title = 'Actualizar recinto: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Recintos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';

/*
 * 
 *  Vista para actualizar un recinto
 * 
 */

?>
<div class="recintos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
