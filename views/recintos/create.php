<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recintos */

$this->title = 'Crear recinto';
$evento = Yii::$app->getRequest()->getQueryParam('evento');
$nomevento = Yii::$app->getRequest()->getQueryParam('nomevento');
if($evento !== null and $nomevento !== null){
    $this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['eventos/index']];
    $this->params['breadcrumbs'][] = ['label' => $nomevento, 'url' => ['eventos/view', 'id' => $evento]];
    $this->params['breadcrumbs'][] = ['label' => 'Asignar ubicación', 'url' => ['alquileres/create2', 'evento' => $evento]];
}else {
    $this->params['breadcrumbs'][] = ['label' => 'Recintos', 'url' => ['index']];
}


$this->params['breadcrumbs'][] = $this->title;

/*
 * 
 * Vista para crear un recinto
 * 
 */
?>
<div class="recintos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
