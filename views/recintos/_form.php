<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use borales\extensions\phoneInput\PhoneInput;

/* @var $this yii\web\View */
/* @var $model app\models\Recintos */
/* @var $form yii\widgets\ActiveForm */

/*
 * 
 *  Formulario para crear o actualizar un recinto
 * 
 */
?>

<div class="recintos-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        
        <div class="col-md-8">
            
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
            
            <?= $form->field($model, 'tipo')->textInput(['maxlength' => true]) ?>
            
        </div>
        
        <div class="col-md-4">
            
            <?= $form->field($model, 'aforo')->textInput() ?>
    
            <?=$form->field($model, 'telefono')->widget(PhoneInput::className(), [
            'jsOptions' => ['preferredCountries' => ['es'],]])?>
            
        </div>
        
        <div class="col-md-12">
            
            <?= $form->field($model, 'ubicacion')->textInput(['maxlength' => true]) ?>
            
        </div>
          
        
    </div>

    <!--<?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>-->

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-lxt']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
