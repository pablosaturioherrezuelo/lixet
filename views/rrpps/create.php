<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rrpps */

$this->title = 'Crear RRPP';
$this->params['breadcrumbs'][] = ['label' => 'Rrpps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

/*
 * 
 * Vista para crear un RRPP
 * 
 */
?>
<div class="rrpps-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
