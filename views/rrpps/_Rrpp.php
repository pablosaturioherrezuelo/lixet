<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/*
 * 
 *  Vista que muestra un rrpp contratado en un evento y la cantidad de entradas que dispone
 * 
 */

$clave = $model->nombre;
$rrpp = $model->id;
$evento = $model->evento;
// Recibe el nombre del evento desde el parametro del controlador
$nomevento = Yii::$app->getRequest()->getQueryParam('nomevento');
$nomrrpp = $model->nombre . ' ' . $model->apellidos;
?>
<!-- <?= var_dump($model) ?> -->
<div class="card sombrabox" style="margin-top: 10px; margin-bottom: 20px">
    <div class="card-body">
        
        <div class="caption text-center">
            <h4><?= $model->nombre . " " .$model->apellidos?></h4>
            <h6>Total entradas:<br><?= $model->numentradas ?></h6>
            <div class="row text-center rrpps">
                <div class="col-md-6" style="padding-bottom: 10px">
                    <?= Html::a('Entradas', ['/entradas/entradasrrpp', 'rrpp' => $rrpp, 'evento' => $evento, 'nomrrpp' => $nomrrpp,'nomevento' => $nomevento], ['class' => 'btn btn-lxt sombrabox']) ?>
                </div>
                <div class="col-md-6">
                    <?= Html::a('Contratos', ['/contratos/contratos', 'rrpp' => $rrpp, 'nomrrpp' => $nomrrpp, 'evento' => $evento, 'nomevento' => $nomevento], ['class' => 'btn btn-outline-lxt sombrabox']) ?>
                </div>
            </div>
        </div>

    </div>
</div> 