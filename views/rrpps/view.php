<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Rrpps */

/*
 * 
 * Vista detallada de un RRPP
 * 
 */
$rol = isset(array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]) ? array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]:'';
$this->title = $model->nombre . ' ' . $model->apellidos;
$this->params['breadcrumbs'][] = ['label' => 'Rrpps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
$borrar = "";
if($rol == "Admin"){
    $borrar = Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Esta seguro de querer borrar este elemento?',
                'method' => 'post',
            ],
        ]);
}
?>
<div class="rrpps-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-lxt']) ?>
        <?= $borrar ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-hover table-responsive-md'],
        'attributes' => [
            //'id',
            'nombre',
            'apellidos',
            'f_nacimiento',
            'dni',
            'email:email',
            'telefono',
        ],
    ]) ?>

</div>
