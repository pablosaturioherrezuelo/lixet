<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rrpps */

/*
 * 
 * Vista para actualizar un RRPP
 * 
 */

$this->title = 'Actualizar RRPP: ' . $model->nombre . ' ' . $model->apellidos;
$this->params['breadcrumbs'][] = ['label' => 'Rrpps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre . ' ' . $model->apellidos, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="rrpps-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
