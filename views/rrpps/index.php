<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

/*
 * 
 * Vista de todos los RRPP dados de alta
 * 
 */

$this->title = 'RRPPS';
$this->params['breadcrumbs'][] = $this->title;

// Se obtiene el rol del usuario registrado para mostrarle sus acciones permitidas
$rol = isset(array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]) ? array_keys(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId()))[0]:'';
$boton = "";
$contrato = [
                'header' => '',
                'content' => function($model) {
                    return "";
                }  
            ];
$actioncolumn = [
                'header' => '',
                'content' => function($model) {
                    return "";
                }  
            ];
            
if($rol == "Admin"){
    // Permisos totales para ver, crear, actualizar y borrar
    $boton = Html::a('Crear nuevo RRPP', ['create'], ['class' => 'btn btn-lxt']);
    $contrato = [
                'header' => '',
                'content' => function($model) {
                    return Html::a('Contratos',['/contratos/contratos','rrpp'=>$model->id, 'nomrrpp'=>$model->nombre, 'from'=>'rrpps'], ['class' => 'btn btn-lxt']);
                }  
            ];
    $actioncolumn = [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ];
} else if($rol == "Jefe"){
    // Solo puede crear y actualizar
    $boton = Html::a('Crear nuevo RRPP', ['create'], ['class' => 'btn btn-lxt']);
    $contrato = [
                'header' => '',
                'content' => function($model) {
                    return Html::a('Contratos',['/contratos/contratos','rrpp'=>$model->id, 'nomrrpp'=>$model->nombre, 'from'=>'rrpps'], ['class' => 'btn btn-lxt']);
                }  
            ];
    $actioncolumn = [
                'header' => '',
                'content' => function($model) {
                    return Html::a('Actualizar',['/rrpps/update','id'=>$model->id,], ['class' => 'btn btn-lxt']);
                }  
            ];
}

?>
<div class="rrpps-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= $boton ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-hover table-responsive-md'],
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'id',
            'nombre',
            'f_nacimiento',
            'dni',
            //'apellidos',
            'email:email',
            'telefono',
            $contrato,
            $actioncolumn,
        ],
    ]); ?>


</div>
