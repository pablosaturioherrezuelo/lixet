<?php
use yii\helpers\Html;
use yii\helpers\HTMLPurifier;
use circulon\widgets\ColumnListView;

/*
 * 
 *  Vista que muestra los rrpps contratados en un evento
 * 
 */

$name = Yii::$app->getRequest()->getQueryParam('nomevento');
$evento = Yii::$app->getRequest()->getQueryParam('evento');
$titulo = "RRPPS activos en " . $name;
$this->title = $titulo;
$this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['eventos/index']];
$this->params['breadcrumbs'][] = ['label' => $name, 'url' => ['eventos/view', 'id' => $evento]];
$this->params['breadcrumbs'][] = 'RRPPS';
?>

<div class="well well-sm text-center">
    <h1 style="">
        <?=$titulo?>
    </h1>
</div>

<div>
    <?= Html::a('Asignar RRPPS', ['/contratos/eventorrpp', 'evento' => $evento, 'nomevento' => $name], ['class' => 'btn btn-lxt sombrabox']) ?>
</div>

<div class="">
    <?= ColumnListView::widget([ 
        'dataProvider' => $dataProvider,
        'itemView' => '_Rrpp',
        'layout'=> "\n{pager}\n{items}",
        'columns' => 2,
    ]);
    ?>
</div>