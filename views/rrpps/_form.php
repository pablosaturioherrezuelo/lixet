<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use borales\extensions\phoneInput\PhoneInput;

/* @var $this yii\web\View */
/* @var $model app\models\Rrpps */
/* @var $form yii\widgets\ActiveForm */

/*
 * 
 * Formulario para crear o actualizar un RRPP
 * 
 */
?>

<div class="rrpps-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        
        <div class="col-md-8">
            
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>
            
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            
        </div>
        
        <div class="col-md-4">
            <!-- Codigo de pais telefonico por defecto españa -->
            <?=$form->field($model, 'telefono')->widget(PhoneInput::className(), [
                'jsOptions' => ['preferredCountries' => ['es'],]])?>
            
            <?= $form->field($model, 'f_nacimiento')->input('date')?>
            
            <?= $form->field($model, 'dni')->textInput(['maxlength' => true]) ?>
            
        </div>
        
    </div>
    
    <!--<?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>-->

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-lxt']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
