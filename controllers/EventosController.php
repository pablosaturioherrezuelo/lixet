<?php

namespace app\controllers;

use app\models\Eventos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * EventosController implements the CRUD actions for Eventos model.
 */
class EventosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Eventos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Eventos::find()//->orderBy('id desc'),
            /*
            'pagination' => [
                'pageSize' => 50
            ],*/
            ,'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Eventos model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Eventos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    /*public function uploadPath() {
        return Url::to('@web/img/');
    }*/
    
    public function upload($fil){
        /*
         * Permite subir una imagen de portada del evento
         */
        $var = 'img/eventos/' . $this->request->bodyParams["Eventos"]["codigo"];
        if ($fil) {
            $this->deleteFile($this->request->bodyParams["Eventos"]["codigo"], '');
            foreach ($fil as $file) {
                $file->saveAs($var .'.'. $file->extension);
            }   
        }
    }
    
    public function uploadHeader($fil) {
        /*
         * Permite subir una imagen de cabecera del evento
         */
        $var = 'img/eventos/' . $this->request->bodyParams["Eventos"]["codigo"]. '-home';
        if ($fil) {
            $this->deleteFile($this->request->bodyParams["Eventos"]["codigo"], '-home');
            foreach ($fil as $file) {
                $file->saveAs($var .'.'. $file->extension);
            }   
        }
    }
    
    public function deleteFile($code, $position) {
        /*
        * No usar (Incompleto) - Borra una imagen recibida como parametro
        */
        $img = getcwd() . '/img/eventos/' . $code;
        if($position != ''){
            $img = $img . $position;
        }
        
        //if(file_exists($img . '.jpg') || fileExists($img . '.png')){
          //unlink(Yii::getAlias('@root') . '/img/eventos/'. $img . '.jpg');
          //unlink($img . '.jpg');
          //unlink($img . '.png');
        //} 
    }

    public function actionCreate()
    {
        $model = new Eventos();
        
        if ($this->request->isPost) {
            // Agrega al modelo los archivos subidos en el formulario
            $model->file = UploadedFile::getInstances($model, 'file');
            $model->filemain = UploadedFile::getInstances($model, 'filemain');
            // Sube los archivos del modelo a web/img/eventos
            $this->upload($model->file, $model->filemain);
            $this->uploadHeader($model->filemain);
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    public function actionCreatebak()
    {
        /*
         * Copia de seguridad de actionCreate
         */
        $model = new Eventos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Eventos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            // Agrega al modelo los archivos subidos en el formulario
            $model->file = UploadedFile::getInstances($model, 'file');
            $model->filemain = UploadedFile::getInstances($model, 'filemain');
            // Sube los archivos del modelo a web/img/eventos
            $this->upload($model->file, $model->filemain);
            $this->uploadHeader($model->filemain);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Eventos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Eventos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Eventos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
        if (($model = Eventos::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    /*public function actionUpload($id){
        $model = $this->findModel($id);
        if(Yii::$app->request->ispost){
            $model->imagen = UploadedFile::getInstance($model, 'imagen');
            if($model->upload()) {
                return $this->redirect(['view', 'id' => $model->id]);
                $model->up
            }
        }
        return $this->render('upload', ['model' => $model]);
    }*/
    
    public function actionMiseventos()
    {
        /*
        *  Devuelve los eventos en los que este contratado el usuario que esta utilizando el sistema
        */
        $dataProvider = new ActiveDataProvider([
            'query' => Eventos::find()
                //->innerJoinWith('contratos')
                ->leftJoin('contratos', 'contratos.evento = eventos.id')
                //->leftJoin('alquileres', $on = 'eventos.id = alquileres.evento')
                ->select('eventos.id, eventos.codigo, eventos.edicion, eventos.nombre, eventos.tipo')
                ->where("contratos.rrpp = (SELECT IdRRPP FROM User WHERE Id =" . \Yii::$app->user->identity->id .")")
                ,
            
            /*'pagination' => [
                'pageSize' => 50
            ],
            */
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            
        ]);

        return $this->render('miseventos', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
}
