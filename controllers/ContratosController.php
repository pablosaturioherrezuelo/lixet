<?php

namespace app\controllers;

use app\models\Contratos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ContratosController implements the CRUD actions for Contratos model.
 */
class ContratosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Contratos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        /*
        * Muestra todos los Contratos y los datos relevantes de sus tablas. Elnombre y apellidos del rrpp los muestra concatenados
        */
        $dataProvider = new ActiveDataProvider([
            'query' => Contratos::find()->innerJoinWith('rrpp0')->innerJoinWith('evento0')
                ->select(['contratos.id id, eventos.nombre nomevento, rrpps.id rrpp, eventos.id evento, concat(rrpps.nombre,rrpps.apellidos) nomrrpp']),
            
            'sort' => ['attributes' => ['nomevento','nomrrpp']],
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Contratos model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->actionRelacionar($id)
        ]);
    }
    
    public function actionViewfromevento($id)
    {
        return $this->render('viewfromevento', [
            'model' => $this->actionRelacionar($id)
        ]);
    }
    
    private function actionRelacionar($id) {
        /*
        * Obtiene valores de sus tablas relacionadas y se lo asigna al modelo.
        */
        $model = $this->findModel($id);
        $model->nomevento = $model->evento0->nombre .' (' . $model->evento0->codigo .'-'. $model->evento0->edicion .')';
        $model->nomrrpp = $model->rrpp0->nombre .' ' . $model->rrpp0->apellidos;
        return $model;
    }

    /**
     * Creates a new Contratos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        
        $model = new Contratos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    public function actionRrppevento($rrpp)
    {
        /*
        * Muestra un formulario para crear un contrato con el rrpp que recibe como parametro como predeterminado
        */
        $model = new Contratos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['contratos', 'rrpp' => $rrpp, 'nomrrpp' => $model->rrpp0->nombre . " " . $model->rrpp0->apellidos, 'evento' => $model->evento, 'nomevento' => $model->evento0->nombre]);
            }
        } else {
            $model->loadDefaultValues();
            $model->rrpp = $rrpp;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    public function actionEventorrpp($evento, $nomevento)
    {
        /*
        * Muestra un formulario para crear un contrato con el evento que recibe como parametro como predeterminado
        */
        $model = new Contratos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['rrpps/rrpps', 'evento' => $evento, 'nomevento' => $nomevento]);
            }
        } else {
            $model->loadDefaultValues();
            $model->evento = $evento;
        }

        return $this->render('createfromeventos', [
            'model' => $model,
        ]);
    }
    
    /*Prueba Fallida para crear varios contratos en un mismo formulario- No usar*/
    public function actionInsertar($evento, $rrpps){
        
        $post = Yii::$app->request->post();

        for ($i=0; $i<count($rrpps); $i++){
          $data[$i][0] = $evento;
          $data[$i][1] = $rrpps[$i]; 
        }

        Yii::$app->db->createCommand()->batchInsert('contratos',[
          'evento',
          'rrpp',
        ],
        $data
        )->execute();
        
    }

    /**
     * Updates an existing Contratos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    public function actionUpdatefromeventos($id)
    {
        /*
         * Renderiza una vista con las migas de pan necesarias para que la navegacion sea natural. 
         * Por lo demas es igual que la funcion action update.
         */
        $model = $this->actionRelacionar($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['viewfromevento', 'id' => $model->id]);
        }

        return $this->render('updatefromeventos', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Contratos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    /*No usar*/
    public function actionDeletefromeventos($id, $evento, $nomevento)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['rrpps/rrpps', 'evento' => $evento, 'nomevento' => $nomevento]);
    }
    
    public function actionDeletefromrrpps($id, $rrpp, $nomrrpp)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['contratos', 'rrpp' => $rrpp, 'nomrrpp' => $nomrrpp, 'from'=>'rrpps']);
    }
    /**
     * Finds the Contratos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Contratos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contratos::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    
    public function actionContratos($rrpp) {
       
        /*
        * Muestra todos los eventos en los que esta activo el rrpp que recibe como parametro
        */
        
        $dataProvider = new ActiveDataProvider([
            'query' => Contratos::find()->innerJoinWith("evento0")->innerJoinWith("rrpp0")
                ->where("contratos.rrpp = $rrpp")->
                select(["contratos.id id, contratos.evento evento, eventos.nombre nomevento, eventos.codigo codigo, concat(rrpps.nombre, ' ', rrpps.apellidos)nomrrpp"])
        ]);
        
        return $this->render("contratosrrpp",[
            "dataProvider" => $dataProvider,
        ]);
    }
    
    public function actionIndexevento($evento)
    {
        /*
         * Muestra todos los RRPP que esten contratados en un evento recibido como parametro
         */
        $dataProvider = new ActiveDataProvider([
            'query' => Contratos::find()
                ->innerJoinWith('rrpp0')->innerJoinWith('evento0')
                ->select(['contratos.id id, eventos.nombre nomevento, rrpps.id rrpp, eventos.id evento, concat(rrpps.nombre,rrpps.apellidos) nomrrpp'])
                ->where("contratos.evento = $evento"),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
