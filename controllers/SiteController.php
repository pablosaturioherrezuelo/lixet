<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Eventos;
use yii\data\ActiveDataProvider;
use app\models\FormUpload;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        /*
         * Muestra la página principal con los 4 ultimos eventos dados de alta en el sistema
         */
        $dataProvider = new ActiveDataProvider([
            'query' => Eventos::find()
            //->innerJoin('grupos', 'artistas.idgrupo = grupos.idgrupo')
            ->select('eventos.nombre, eventos.id, eventos.edicion, eventos.codigo')
            ->orderBy('eventos.id desc')->groupBy('eventos.id')->having('eventos.id > (select count(*) - 4 from eventos)'),
        ]);
        
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            // Comprueba los roles del usuario
            $rol = isset(array_keys(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId()))[0]) ? array_keys(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId()))[0]:'';
            if($rol == ''){
                return $this->actionSinacciones();
            }
            return $this->goBack();
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }
    
    public function actionSinacciones(){
        /*
         * Muestra una página con indicaciones para los usuarios que no tienen un rol asignado. 
         * Tambien muestra el contacto de su jefe en caso de tenerlo asignado
         */
        $id = \Yii::$app->user->identity->idrrpp;
        
        if(isset($id)){
            //$info = \Yii::$app->db->createCommand('SELECT concat("Tu jefe es: ", nombre, " ", apellidos, ".<br> Teléfono: ", telefono ) FROM rrpps inner join jefes on rrpps.id = jefes.jefe WHERE jefes.vendedor = 20')->queryScalar();
            $info = \Yii::$app->db->createCommand('SELECT concat("Tu jefe es: ", nombre, " ", apellidos, ".<br> Teléfono: ", telefono ) FROM rrpps inner join jefes on rrpps.id = jefes.jefe WHERE jefes.vendedor = ' . $id)->queryScalar();     
        } else {
            $info = "";   
        }
        return $this->render('sinacciones',['info'=>$info]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    /**
     * Displays manage page.
     *
     * @return string
     */
    public function actionManage()
    {
        /*
         * pagina para manejar los modelos, usada para mantenimiento
         */
        return $this->render('manage');
    }
    
    public function actionGenerar(){
        /*
         * No usar
         */
        return $this->render('generar');
    }
    
    public function actionUpload() {
        /*
         * NO USAR!! VIEJO. Sube una imagen a la carpeta img
         */
        $model = new FormUpload;
        $msg = null;
  
        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstances($model, 'file');
            //Para un input file simple
            //$model->file = UploadedFile::getInstance($model, 'file');
            //$file = $model->file;
            //$file->saveAs($file, $deleteTempFile)
            if ($model->file && $model->validate()) {
                foreach ($model->file as $file) {
                    $file->saveAs('img/' . $file->baseName . '.' . $file->extension);
                    $msg = "<p><strong class='label label-info'>Enhorabuena, subida realizada con éxito</strong></p>";
                }
            }
        }
        return $this->render("upload", ["model" => $model, "msg" => $msg]);
    }
}
