<?php

namespace app\controllers;

use app\models\Rrpps;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RrppsController implements the CRUD actions for Rrpps model.
 */
class RrppsController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Rrpps models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Rrpps::find()->where('id <> -1')
                ->select(['id, dni, concat(nombre, " ", apellidos) nombre, telefono, email, '
                    . 'DATE_FORMAT(f_nacimiento, "%d-%m-%Y") f_nacimiento']),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Rrpps model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->actionFormatofechas($id),
        ]);
    }
    
    //Dar formato a las fechas
    private function actionFormatofechas($id) {
        /*
         * Devuelve un modelo con las fechas formateadas
         */
        $model = $this->findModel($id);
        $date = new \DateTime($model->f_nacimiento);
        $model->f_nacimiento = $date->format('d/m/Y');
        return $model;
    }
    
    /**
     * Creates a new Rrpps model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Rrpps();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Rrpps model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Rrpps model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Rrpps model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Rrpps the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rrpps::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('La página solicitada no existe.');
    }
    
    //Obtener todos los rrpps activos en un evento y sus entradas
    public function actionRrpps($evento) {
        
        //Consulta sql para obtener tambien los rrpps que no esten activos en el evento pero tengan entradas de este
        $sql = "select rrpps.id, rrpps.nombre, rrpps.apellidos, entradas.evento evento,count(*) numentradas
            from rrpps 
            inner join entradas on entradas.vendedor = rrpps.id 
            where rrpps.id <> -1 and entradas.evento = $evento
            group by rrpps.id";
        
        //RRPPS dados de alta en el evento pasado como parametro y entradas que tienen asignadas
        $dataProvider = new ActiveDataProvider([
            'query' => Rrpps::find()
                ->innerJoinWith("contratos")
                ->where("rrpps.id <> -1 and contratos.evento = $evento")
                ->select(["rrpps.id, rrpps.nombre, rrpps.apellidos, contratos.evento evento,"
                    . "(select count(*)from entradas where entradas.evento = $evento and entradas.vendedor = rrpps.id) numentradas"])
                ->union($sql)
        ]);
        
        return $this->render("Rrpps",[
            "dataProvider" => $dataProvider,
        ]);
    }
    
}
