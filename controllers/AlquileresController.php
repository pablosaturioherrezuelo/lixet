<?php

namespace app\controllers;

use app\models\Alquileres;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AlquileresController implements the CRUD actions for Alquileres model.
 */
class AlquileresController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Alquileres models.
     *
     * @return string
     */
    public function actionIndex()
    {
        /*
        * Muestra todos los Alquileres y los datos relevantes de sus tablas. Tambien formatea campos como las fechas y hora
        */
        $dataProvider = new ActiveDataProvider([
            'query' => Alquileres::find()//->orderBy('id desc')
                ->innerJoinWith('evento0')->innerJoinWith('recinto0')
                ->select(['alquileres.id, eventos.nombre nomevento, recintos.nombre nomrecinto'
                        .', concat(DATE_FORMAT(f_inicio, "%d-%m-%Y"), " - ", substring(h_inicio,1,5)) f_inicio'
                    . ', concat(DATE_FORMAT(f_final, "%d-%m-%Y"), " - ", substring(h_final,1,5)) f_final']), //, (select FORMAT(f_inicio, "dd:MM:yy")) f_inicio'
            
            'sort' => [
                'attributes' => ['id','nomevento','nomrecinto','f_inicio','f_final'],
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ]
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],*/
  
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionIndex2($evento)
    {
        /*
        * Muestra todos los Alquileres y los datos relevantes de sus tablas. Tambien formatea campos como las fechas y hora
        */
        $dataProvider = new ActiveDataProvider([
            'query' => Alquileres::find()//->orderBy('id desc')
                ->innerJoinWith('evento0')->innerJoinWith('recinto0')->where("alquileres.evento = $evento")
                ->select(['alquileres.id, eventos.nombre nomevento, recintos.nombre nomrecinto'
                        .', DATE_FORMAT(f_inicio, "%d-%m-%Y") f_inicio'
                    . ', DATE_FORMAT(f_final, "%d-%m-%Y") f_final']), //, (select FORMAT(f_inicio, "dd:MM:yy")) f_inicio'
            
            'sort' => [
                'attributes' => ['id','nomevento','nomrecinto','f_inicio','f_final'],
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ]
  
        ]);

        return $this->render('indexfromeventos', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Displays a single Alquileres model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {   
        return $this->render('view', [
            'model' => $this->actionRelacionar($id)
        ]);
    }
    
    public function actionViewfromeventos($id)
    {   
        return $this->render('viewfromeventos', [
            'model' => $this->actionRelacionar($id)
        ]);
    }
    
    private function actionRelacionar($id){
        /*
         * Obtiene valores de sus tablas relacionadas y se lo asigna al modelo. Tambien formatea campos como las fechas y horas
         */
        $model = $this->findModel($id);
        $model->nomevento = $model->evento0->nombre .' (' . $model->evento0->codigo .'-'. $model->evento0->edicion .')';
        $model->nomrecinto = $model->recinto0->nombre;
        $fini = isset($model->f_inicio) ? new \DateTime($model->f_inicio) : null;
        $ffin = isset($model->f_final) ? new \DateTime($model->f_final) : null;
        $model->f_inicio = isset($fini) ? $fini->format('d/m/Y') : '';
        $model->f_final = isset($ffin) ? $ffin->format('d/m/Y') : '';
        $model->h_inicio = isset($model->h_inicio) ? $model->h_inicio : '';
        $model->h_final = isset($model->h_final) ? $model->h_final : '';
        $model->telefono = $model->recinto0->telefono;
        $model->tipo = $model->recinto0->tipo;
        $model->aforo = $model->recinto0->aforo;
        $model->ubicacion = $model->recinto0->ubicacion;
        
        return $model;
    }
    
    public function actionViewbak($id)
    {
        /*
         * copia de seguridad de actionView
         */
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Alquileres model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Alquileres();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    public function actionCreate2($evento)
    {
        /*
         * Segunda accion que permite crear un alquiler recibiendo como parametro el evento
         */
        $model = new Alquileres();
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                if($evento != 0){
                    return $this->redirect(['alquileres/viewfromeventos', 'id' => $model->id]);
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
            $model->evento = $evento;
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }
    
    public function actionCreate3($evento,$recinto)
    {
        /*
         * Tercera accion que permite crear un alquiler recibiendo como parametro el evento y recinto
         */
        $model = new Alquileres();
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['alquileres/viewfromeventos', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
            $model->evento = $evento;
            $model->recinto = $recinto;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Alquileres model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionUpdatefromeventos($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['viewfromeventos', 'id' => $model->id]);
        }

        return $this->render('updatefromeventos', [
            'model' => $model,
        ]);
    }
    /**
     * Deletes an existing Alquileres model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDeletefromeventos($id)
    {
        $evento = $this->actionRelacionar($id)->evento;
        $nomevento = $this->actionRelacionar($id)->nomevento;
        $this->findModel($id)->delete();

        return $this->redirect(['index2', 'evento' => $evento, 'nomevento' => $nomevento]);
    }
    
    /**
     * Finds the Alquileres model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Alquileres the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Alquileres::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function findPrueba($id)
    {
        if (($model = \app\models\Eventos::findOne(['id' => $id])) !== null) {
            return $model->nombre;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
}
