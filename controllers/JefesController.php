<?php

namespace app\controllers;

use app\models\Jefes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * JefesController implements the CRUD actions for Jefes model.
 */
class JefesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Jefes models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Jefes::find()->innerJoinWith('jefe0')->innerJoinWith('vendedor0')
                ->select(['jefes.id, jefe, vendedor, (select concat(rrpps.nombre, " ", rrpps.apellidos) from rrpps where jefe = rrpps.id) nomjefe, (select concat(rrpps.nombre, " ", rrpps.apellidos) from rrpps where vendedor = rrpps.id) nomrrpp']),
            
            'sort' => ['attributes' => ['nomjefe','nomrrpp']],
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionIndex2($jefe)
    {
        /*
         * Muestra los rrpp dados de alta en un grupo cuyo jefe es recibido como parametro
         */
        $dataProvider = new ActiveDataProvider([
            'query' => Jefes::find()->innerJoinWith('jefe0')->innerJoinWith('vendedor0')->where("jefes.jefe = $jefe")
                ->select(['jefes.id, jefe, vendedor, (select concat(rrpps.nombre, " ", rrpps.apellidos) from rrpps where jefe = rrpps.id) nomjefe, (select concat(rrpps.nombre, " ", rrpps.apellidos) from rrpps where vendedor = rrpps.id) nomrrpp']),
            
            'sort' => ['attributes' => ['nomjefe','nomrrpp']],
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index2', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Jefes model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->actionRelacionar($id)
        ]);
    }
    
    private function actionRelacionar($id) {
        /*
        * Obtiene valores de sus tablas relacionadas y se lo asigna al modelo. Tambien formatea campos como las fechas y horas
        */
        $model = $this->findModel($id);
        $model->nomjefe = $model->jefe0->nombre .' ' . $model->jefe0->apellidos;
        $model->nomrrpp = $model->vendedor0->nombre .' ' . $model->vendedor0->apellidos;
        return $model;
    }

    /**
     * Creates a new Jefes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate($jefe)
    {
        /*
         * Permite agregar un rrpp a un grupo cuyo jefe es recibido como parametro
         */
        $model = new Jefes();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                $model->jefe = $jefe;
                return $this->redirect(['index2', 'jefe' => $model->jefe, 'nomjefe' => $model->nomjefe]);
            }
        } else {
            $model->loadDefaultValues();
            $model->jefe = $jefe;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    public function actionCreategrupo()
    {
        /*
         * Asigna un rrpp a el grupo del jefe -1 que es el sistema de lixet. Los rrpps que esten en este grupo seran jefes
         */
        $model = new Jefes();       
        $model->jefe = -1;
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues(); 
        }
        
        return $this->render('creategrupo', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Jefes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Jefes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['grupo']);
    }

    /**
     * Finds the Jefes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Jefes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Jefes::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionGrupo() {
        /*
         * Muestra los grupos de rrpp agrupados por jefes cuando estos jefes estan en el grupo del sistema lixet.
         */
        $dataProvider = new ActiveDataProvider([
            'query' => Jefes::find()->innerJoinWith('vendedor0')
                ->groupBy('vendedor')
                ->select(['vendedor jefe, concat(rrpps.nombre, " ", rrpps.apellidos) nomjefe, vendedor jefaso, (SELECT count(*) FROM jefes WHERE jefe = jefaso) integrantes'])
                ->where('jefe = -1'),
        ]);

        return $this->render('grupos', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionMigrupo() {
        /*
        *  Devuelve el grupo al que pertenece el usuario que esta navegando en el sistema
        */
        $union = "SELECT 'RRPP' tipo, vendedor, rrpps.telefono, (select concat(rrpps.nombre, ' ', rrpps.apellidos) from rrpps where vendedor = rrpps.id) as nomrrpp FROM jefes INNER JOIN rrpps on jefes.vendedor = rrpps.id WHERE jefe = (SELECT jefe FROM jefes WHERE vendedor = (SELECT IdRRPP FROM User WHERE Id = " . \Yii::$app->user->identity->id . "))";
        $dataProvider = new ActiveDataProvider([
            'query' => Jefes::find()->innerJoinWith('jefe0')//->innerJoinWith('vendedor0')//->groupBy('jefe')
                ->select(['"Jefe" tipo, jefe, rrpps.telefono, concat(rrpps.nombre, " ", rrpps.apellidos) as nomrrpp'])
                ->where("jefe = (SELECT jefe FROM jefes WHERE vendedor = (SELECT IdRRPP FROM User WHERE Id =" . \Yii::$app->user->identity->id ."))")
                ->union($union),
            'sort' => ['attributes' => ['id','vendedor','nomrrpp','tipo','telefono']],
        ]);

        return $this->render('migrupo', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionJefes()
    {
        /*
         * Muestra el grupo de los jefes de rrpp que esten en grupo del sistema lixet.
         */
        $dataProvider = new ActiveDataProvider([
            'query' => Jefes::find()->innerJoinWith('vendedor0')
                ->select(['jefes.id, (select concat(rrpps.nombre, " ", rrpps.apellidos) from rrpps where vendedor = rrpps.id) nomjefe'])
                ->where('jefe=-1'),
            
            'sort' => ['attributes' => ['nomjefe']],
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('jefes', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
