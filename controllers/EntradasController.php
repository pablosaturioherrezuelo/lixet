<?php

namespace app\controllers;

use app\models\Entradas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db;
/**
 * EntradasController implements the CRUD actions for Entradas model.
 */
class EntradasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Entradas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        /*
         * Devuelve tas las entradas del sistema formateando campos
         */
        $dataProvider = new ActiveDataProvider([
            'query' => Entradas::find()
                ->leftJoin('clientes', $on = 'clientes.id = comprador')->leftJoin('rrpps', $on = 'rrpps.id = vendedor')->innerJoinWith('evento0')
                ->leftJoin('tipos_entradas', $on = 'tipos_entradas.evento = entradas.evento and tipos_entradas.numero = entradas.numero')
                //->innerJoinWith('comprador0')->innerJoinWith('vendedor0')->innerJoinWith('evento0')
                ->select(['entradas.id','concat(eventos.nombre, " ", eventos.edicion) nomevento', 'entradas.numero, eventos.id idevento, entradas.precio, tipos_entradas.tipo, '
                    . 'concat(clientes.nombre, " ", clientes.apellidos) nomcomprador',
                    'concat(rrpps.nombre, " ", rrpps.apellidos) nomvendedor','concat(DATE_FORMAT(entradas.fecha, "%d-%m-%Y"), " - ", substring(entradas.hora,1,5)) fecha,'
                    . 'concat(eventos.codigo, eventos.edicion, "-", entradas.numero) numentrada'])
                //->orderBy('entradas.evento desc, entradas.numero asc')
                //->where("vendedor = (SELECT IdRRPP FROM User WHERE Id =" . \Yii::$app->user->identity->id .")")
            ,
            'sort' => ['attributes' => ['idevento','numero','nomevento','numentrada','precio','fecha','nomcomprador','nomvendedor', 'tipo'],
                'defaultOrder' => [
                    'idevento' => SORT_DESC,
                    'numero' => SORT_ASC,
                ],
            ],
            
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionIndexfromeventos($evento)
    {
        /*
         * Devuelve tas las entradas de un evento
         */
        $dataProvider = new ActiveDataProvider([
            'query' => Entradas::find()
                ->leftJoin('clientes', $on = 'clientes.id = comprador')->leftJoin('rrpps', $on = 'rrpps.id = vendedor')->innerJoinWith('evento0')
                ->leftJoin('tipos_entradas', $on = 'tipos_entradas.evento = entradas.evento and tipos_entradas.numero = entradas.numero')
                //->innerJoinWith('comprador0')->innerJoinWith('vendedor0')->innerJoinWith('evento0')
                ->select(['entradas.id','concat(eventos.nombre, " ", eventos.edicion) nomevento', 'entradas.numero, eventos.id idevento, entradas.precio, tipos_entradas.tipo, '
                    . 'concat(clientes.nombre, " ", clientes.apellidos) nomcomprador',
                    'concat(rrpps.nombre, " ", rrpps.apellidos) nomvendedor','concat(DATE_FORMAT(entradas.fecha, "%d-%m-%Y"), " - ", substring(entradas.hora,1,5)) fecha,'
                    . 'concat(eventos.codigo, eventos.edicion, "-", entradas.numero) numentrada'])
                //->orderBy('entradas.evento desc, entradas.numero asc')
                ->where("entradas.evento = $evento")
            ,
            'sort' => ['attributes' => ['idevento','numero','nomevento','numentrada','precio','fecha','nomcomprador','nomvendedor', 'tipo'],
                'defaultOrder' => [
                    'idevento' => SORT_DESC,
                    'numero' => SORT_ASC,
                ],
            ],
        ]);

        return $this->render('indexfromeventos', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Entradas model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            //'model' => $this->findModel($id),
            'model' => $this->actionRelacionar($id),
        ]);
    }
    
    public function actionViewfromeventos($id)
    {
        /*
         * Renderiza una vista que muestra un modelo recibido como parametro.
         * Usado para tener unas migas de pan acordes si el usuario navega desde eventos
         */
        return $this->render('viewfromeventos', [
            //'model' => $this->findModel($id),
            'model' => $this->actionRelacionar($id),
        ]);
    }
    
    private function actionRelacionar($id){
        /*
        * Obtiene valores de sus tablas relacionadas y se lo asigna al modelo. Tambien formatea campos como las fechas y horas
        */
        $model = $this->findModel($id);
        $model->nomevento = $model->evento0->nombre .' (' . $model->evento0->codigo .'-'. $model->evento0->edicion .')';
        $model->nomcomprador = isset($model->comprador) ? $model->comprador0->nombre .' ' . $model->comprador0->apellidos:'No vendida';
        $model->nomvendedor = isset($model->vendedor) ? $model->vendedor0->nombre .' ' . $model->vendedor0->apellidos:'Sin vendedor';
        //$model->nomcomprador = $model->comprador0->nombre .' ' . $model->comprador0->apellidos;
        //$model->nomvendedor = $model->vendedor0->nombre .' ' . $model->vendedor0->apellidos;
        $model->tipo = !empty($model->tiposEntradas->tipo) ? $model->tiposEntradas->tipo : 'Sin variante de tipo';
        $date = new \DateTime($model->fecha);
        $time = !empty($model->hora) ? \Yii::$app->formatter->asDate($model->hora,'H:i') : 'Asignar hora';
        $model->hora = \Yii::$app->formatter->asDate($model->hora,'H:i');
        $date1 = !empty($model->fecha) ? $date->format('d/m/y') : 'Asignar fecha';
        $model->fech = $date1 . ' - ' . $time;
        return $model;
    }
    
    /**
     * Creates a new Entradas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        /*
        * Genera la cantidad de entradas indicadas en el formulario
        * Tambien asigna un tipo de entrada
        * Llama a un procedimiento de la BBDD para esta generacion llamado generar_entradas
        */     
        $model = new Entradas();
        $model->numero = 0;
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                \Yii::$app->db->createCommand(
                    "CALL generar_entradas(:evento, :cantidad, :precio, :comision, :tipo)")
                    ->bindValue(':evento' , $model->evento )
                    ->bindValue(':cantidad', $model->cantidad)
                    ->bindValue(':precio' , $model->precio)
                    ->bindValue(':comision', $model->comision)
                    ->bindValue(':tipo', $model->tipo)->execute();
                //return $this->redirect(['view', 'id' => $model->id]);
                return $this->redirect('index');
            }
        } else {
            $model->loadDefaultValues();
            $model->numero = 0;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    public function actionCreate2($evento)
    {
        /*
         * NO USAR ES VIEJO - Crea una entrada y asigna su evento recibido como parametro
         */
        $model = new Entradas();
        $model->numero = 0;
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
            $model->evento = $evento;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    public function actionGenerar($evento)
    {
        /*
         * Genera la cantidad de entradas indicadas en el formulario para un evento recibido como parametro
         * Tambien asigna un tipo de entrada
         * Llama a un procedimiento de la BBDD para esta generacion llamado generar_entradas
         */        
        $model = new Entradas();
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                \Yii::$app->db->createCommand(
                    "CALL generar_entradas(:evento, :cantidad, :precio, :comision, :tipo)")
                    ->bindValue(':evento' , $model->evento )
                    ->bindValue(':cantidad', $model->cantidad)
                    ->bindValue(':precio' , $model->precio)
                    ->bindValue(':comision', $model->comision)
                    ->bindValue(':tipo', $model->tipo)->execute();
                return $this->redirect(['indexfromeventos', 'evento' => $evento, 'nomevento' => $model->evento0->nombre]);    
            }
        } else {
            $model->loadDefaultValues();
            $model->evento = $evento;
            $model->numero = 0;
        }

        return $this->render('generarentrada', [
            'model' => $model,
        ]);
    }
    
    public function actionCrearentrada($evento, $nomevento)
    {
        /*
         * NO USAR ES VIEJO - Crea una entrada y asigna su evento recibido como parametro
         */
        $model = new Entradas();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                //return $this->redirect(['view', 'id' => $model->id]);
                return $this->redirect(['indexfromeventos', 'evento' => $evento, 'nomevento' => $nomevento]);
            }
        } else {
            $model->loadDefaultValues();
            $model->evento = $evento;
        }

        return $this->render('crearentrada', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Entradas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        /*
         * Actualiza una entrada y formatea su tipo
         * Llama a un procedimiento de la BBDD para esta actualizacion llamado actualizar_entradas 
         * Tambien actualiza el tipo de entrada
         */
        $model = $this->actionRelacionar($id);
        $model->tipo = !empty($model->tiposEntradas->tipo) ? $model->tiposEntradas->tipo : 'Sin variante de tipo';
        $model->cantidad = 1;

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
           \Yii::$app->db->createCommand(
                    "CALL actualizar_entradas(:evento, :numero, :tipo)")
                    ->bindValue(':evento' , $model->evento)
                    ->bindValue(':numero', $model->numero)
                    ->bindValue(':tipo', $model->tipo)->execute();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    public function actionUpdatefromeventos($id)
    {
        /*
         * Actualiza una entrada y formatea su tipo desde un evento
         * Llama a un procedimiento de la BBDD para esta actualizacion llamado actualizar_entradas
         * Tambien actualiza el tipo de entrada
         * Utilizado para respetar las migas de pan y tener una navegación optima
         */
        $model = $this->findModel($id);
        $model->tipo = !empty($model->tiposEntradas->tipo) ? $model->tiposEntradas->tipo : 'Sin variante de tipo';
        $model->cantidad = 1;

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            \Yii::$app->db->createCommand(
                    "CALL actualizar_entradas(:evento, :numero, :tipo)")
                    ->bindValue(':evento' , $model->evento)
                    ->bindValue(':numero', $model->numero)
                    ->bindValue(':tipo', $model->tipo)->execute();
            return $this->redirect(['viewfromeventos', 'id' => $model->id]);
        }

        return $this->render('updatefromeventos', [
            'model' => $model,
        ]);
    }
    
    public function actionUpdatefromentradasrrpp($id, $rrpp, $nomrrpp)
    {
        /*
         * Asigna una entrada a un rrpp recibido como parametro
         */
        $model = $this->actionRelacionar($id);
        $model->vendedor = $rrpp;
        $model->cantidad = 1;
        
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['asignarentradas', 'evento' => $model->evento, 'nomevento' => $model->nomevento, 
                'rrpp'=>$rrpp,'nomrrpp' => $nomrrpp]);
        }
        
        return $this->render('updatefromentradasrrpp', [
            'model' => $model,
        ]);
    }
    
    public function actionCambiarvendedor($id, $rrpp, $nomrrpp)
    {
        /*
         * Cambia el vendedor de una entrada recibido como parametro
         */
        $model = $this->actionRelacionar($id);
        //$model->vendedor = $rrpp;
        $model->cantidad = 1;
        
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['entradasrrpp', 'evento' => $model->evento, 'nomevento' => $model->nomevento, 
                'rrpp'=>$rrpp,'nomrrpp' => $nomrrpp]);
        }
        
        return $this->render('cambiarvendedor', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Entradas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDeletefromeventos($id)
    {
        /*
         * Borra una entrada desde un evento. Utilizado para respetar las migas de pan y tener una navegación optima
         */
        $evento = $this->actionRelacionar($id)->evento;
        $nomevento = $this->actionRelacionar($id)->nomevento;
        $this->findModel($id)->delete();

        return $this->redirect(['indexfromeventos', 'evento' => $evento, 'nomevento' => $nomevento]);
    }

    /**
     * Finds the Entradas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Entradas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Entradas::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionEntradasrrpp($rrpp,$evento) {
       /*
        * Muestra las entradas que dispone un rrpp en un evento ambos recibidos como parametro
        */
        $dataProvider = new ActiveDataProvider([
            'query' => Entradas::find()->innerJoinWith("evento0")
                ->leftJoin('tipos_entradas', $on = 'tipos_entradas.evento = entradas.evento and tipos_entradas.numero = entradas.numero')
                ->where("entradas.evento = $evento and entradas.vendedor = $rrpp")
                ->select(["entradas.id, entradas.vendedor, entradas.comprador, entradas.numero, eventos.codigo,concat(eventos.codigo, '-', eventos.edicion) nomevento, entradas.precio, entradas.comision, tipos_entradas.tipo"])
                //->orderBy('numero asc')
            ,'sort' => [
                'defaultOrder' => [
                    'numero' => SORT_ASC,
                ]
            ],
        ]);
        
        return $this->render("entradasrrpp",[
            "dataProvider" => $dataProvider,
        ]);
    }
    
    public function actionAsignarentradas($rrpp,$evento,$nomrrpp,$nomevento) {
       /*
        * Muestra todas las entradas de un evento y sin comprador con una opcion para asignar a un rrpp
        */
       $dataProvider = new ActiveDataProvider([
            'query' => Entradas::find()
               ->where("entradas.evento = $evento and entradas.comprador is null")
                ->leftJoin('clientes', $on = 'clientes.id = comprador')->leftJoin('rrpps', $on = 'rrpps.id = vendedor')->innerJoinWith('evento0')
               ->leftJoin('tipos_entradas', $on = 'tipos_entradas.evento = entradas.evento and tipos_entradas.numero = entradas.numero')
                //->innerJoinWith('comprador0')->innerJoinWith('vendedor0')->innerJoinWith('evento0')
                ->select(['entradas.id','concat(eventos.nombre, " ", eventos.edicion) nomevento', 'entradas.numero, entradas.precio, tipos_entradas.tipo, '
                    . 'concat(clientes.nombre, " ", clientes.apellidos) nomcomprador',
                    'concat(rrpps.nombre, " ", rrpps.apellidos) nomvendedor','concat(DATE_FORMAT(entradas.fecha, "%d-%m-%Y"), " - ", substring(entradas.hora,1,5)) fecha,'
                    . 'concat(eventos.codigo, eventos.edicion, "-", entradas.numero) numentrada'])
            ,
            'sort' => ['attributes' => ['nomevento','numentrada','precio','fecha','nomcomprador','nomvendedor','tipo']],
        ]);
        
        return $this->render("indexasignarentradas",[
            "dataProvider" => $dataProvider,
        ]);
    }
    
    public function actionMisentradas(){
        /*
        *  Devuelve las entradas que disponga el usuario
        */
        $dataProvider = new ActiveDataProvider([
            'query' => Entradas::find()
            ->leftJoin('clientes', $on = 'clientes.id = comprador')->innerJoinWith('evento0')->leftJoin('alquileres', $on = 'alquileres.evento = eventos.id')
            ->leftJoin('tipos_entradas', $on = 'tipos_entradas.evento = entradas.evento and tipos_entradas.numero = entradas.numero')
            //->innerJoinWith('comprador0')->innerJoinWith('vendedor0')->innerJoinWith('evento0')
            ->select(['entradas.id','concat(eventos.nombre, " ", eventos.edicion) nomevento', 'entradas.numero, eventos.codigo, entradas.precio, tipos_entradas.tipo, '
                . 'concat(clientes.nombre, " ", clientes.apellidos) nomcomprador',
                'concat(DATE_FORMAT(entradas.fecha, "%d-%m-%Y"), " - ", substring(entradas.hora,1,5)) fecha,'
                . 'concat(eventos.codigo, eventos.edicion, "-", entradas.numero) numentrada'])
            ->where("vendedor = (SELECT IdRRPP FROM User WHERE Id =" . \Yii::$app->user->identity->id .")"
                    . " and ((SELECT MAX(f_final) FROM alquileres WHERE entradas.evento = alquileres.evento GROUP BY alquileres.evento) IS NULL OR (SELECT MAX(f_final) FROM alquileres WHERE entradas.evento = alquileres.evento GROUP BY alquileres.evento) >= '". date('d-m-Y')."')")
            ,
            //'sort' => ['attributes' => ['nomevento','numentrada','precio','fecha','nomcomprador','nomvendedor', 'tipo']],
            
            /*
            'pagination' => [
                'pageSize' => 50
            ],*/
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
            
        ]);

        return $this->render('misentradas', [
            'dataProvider' => $dataProvider,
        ]);
        
    }
    
    public function actionVenderentrada($id)
    {
        /*
         * Vende una entrada.
         * Se considera venta cuando se asigna comprador. Se asigna la fecha y hora de la venta automaticamente.
         */
        $model = $this->actionRelacionar($id);
        $model->cantidad = 1;
        $model->fecha = \Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
        $model->hora = \Yii::$app->formatter->asDate('now','H:i');
        
        
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            if(empty($model->comprador)){
                $model->fecha = null;
                $model->hora = null;
                $model->save();
            }
            return $this->redirect(['misentradas']);
        }
        
        return $this->render('venderentrada', [
            'model' => $model,
        ]);
    }
    
    public function actionVenderentradaevento($id)
    {
        /*
         * Vende una entrada desde una vista de un evento.
         * Se considera venta cuando se asigna comprador. Se asigna la fecha y hora de la venta automaticamente.
         */
        $model = $this->actionRelacionar($id);
        $model->cantidad = 1;
        $model->fecha = \Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
        $model->hora = \Yii::$app->formatter->asDate('now','H:i');
        
        
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            if(empty($model->comprador)){
                $model->fecha = null;
                $model->hora = null;
                $model->save();
            }
            return $this->redirect(['misentradaseventos','evento'=> $model->evento,'nomevento' => $model->nomevento]);
        }
        
        return $this->render('venderentradaevento', [
            'model' => $model,
        ]);
    }
    
    public function actionMisentradaseventos($evento){
        /*
        *  Devuelve las entradas que disponga el usuario en un evento recibido como parametro
        */
        $dataProvider = new ActiveDataProvider([
            'query' => Entradas::find()
            ->leftJoin('clientes', $on = 'clientes.id = comprador')->innerJoinWith('evento0')->leftJoin('alquileres', $on = 'alquileres.evento = eventos.id')
            ->leftJoin('tipos_entradas', $on = 'tipos_entradas.evento = entradas.evento and tipos_entradas.numero = entradas.numero')
            //->innerJoinWith('comprador0')->innerJoinWith('vendedor0')->innerJoinWith('evento0')
            ->select(['entradas.id','concat(eventos.nombre, " ", eventos.edicion) nomevento', 'entradas.numero, eventos.codigo, entradas.precio, tipos_entradas.tipo, '
                . 'concat(clientes.nombre, " ", clientes.apellidos) nomcomprador',
                'concat(DATE_FORMAT(entradas.fecha, "%d-%m-%Y"), " - ", substring(entradas.hora,1,5)) fecha,'
                . 'concat(eventos.codigo, eventos.edicion, "-", entradas.numero) numentrada'])
            ->where("entradas.evento = $evento AND vendedor = (SELECT IdRRPP FROM User WHERE Id =" . \Yii::$app->user->identity->id .")"
                    . " and ((SELECT MAX(f_final) FROM alquileres WHERE entradas.evento = alquileres.evento GROUP BY alquileres.evento) IS NULL OR (SELECT MAX(f_final) FROM alquileres WHERE entradas.evento = alquileres.evento GROUP BY alquileres.evento) >= '". date('d-m-Y')."')")
            ,
            //'sort' => ['attributes' => ['nomevento','numentrada','precio','fecha','nomcomprador','nomvendedor', 'tipo']],
            
            /*
            'pagination' => [
                'pageSize' => 50
            ],*/
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
            
        ]);

        return $this->render('misentradaseventos', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionMejoresvendedores(){
        /*
         * Muestra los vendedores que hayan vendido entradas en el sistema
         * Ordena por precio total de ventas, cantidad de entradas y beneficio total generado al promotor
         */
        $dataProvider = new ActiveDataProvider([
            'query' => Entradas::find()
               ->leftJoin('rrpps', $on = 'entradas.vendedor = rrpps.id')
               //->select(['IFNULL(CONCAT(rrpps.nombre, " ", rrpps.apellidos),"Entregado y no vendido") nomvendedor, COUNT(*) as Cantidad, SUM(IFNULL(entradas.comision,0.00)) Comision, SUM(IFNULL(entradas.precio,0.00)) Ventas, (SUM(IFNULL(entradas.precio,0.00)) - SUM(IFNULL(entradas.comision,0.00))) Beneficio'])
               ->select(['IFNULL(CONCAT(rrpps.nombre, " ", rrpps.apellidos),"Entregado y no vendido") nomvendedor, COUNT(*) cantidad, SUM(entradas.comision) comision, SUM(entradas.precio) precio, SUM(entradas.precio)-SUM(entradas.comision) beneficio'])
               ->where('IFNULL(entradas.comprador,"")<>"" AND IFNULL(entradas.vendedor,"")<>""')
               ->groupBy('entradas.vendedor')
            ,'sort' => [
                'attributes' => ['nomvendedor','cantidad','precio','comision','beneficio'],
                'defaultOrder' => [
                    'precio' => SORT_DESC,
                    'cantidad' => SORT_DESC,
                    'beneficio' => SORT_DESC,
                ]
            ],
            
        ]);
        return $this->render('mejoresvendedores', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionMejoresvendedoresevento($evento){
        /*
         * Muestra los vendedores que hayan vendido entradas en un evento recibido como parametro
         * Ordena por precio total de ventas, cantidad de entradas y beneficio total generado al promotor en ese evento
         */
        $total = \Yii::$app->db->createCommand('SELECT concat("Vendidas: ", count(comprador), " / ", count(*)) FROM entradas WHERE entradas.evento = ' . $evento)->queryScalar();
        $dataProvider = new ActiveDataProvider([
            'query' => Entradas::find()
               ->leftJoin('rrpps', $on = 'entradas.vendedor = rrpps.id')
               ->select(['IFNULL(CONCAT(rrpps.nombre, " ", rrpps.apellidos),"Entregado y no vendido") nomvendedor, COUNT(*) cantidad, SUM(entradas.comision) comision, SUM(entradas.precio) precio, SUM(entradas.precio)-SUM(entradas.comision) beneficio'])
               ->where('IFNULL(entradas.comprador,"")<>"" AND IFNULL(entradas.vendedor,"")<>"" and entradas.evento = ' . $evento)
               ->groupBy('entradas.vendedor')
            ,'sort' => [
                'attributes' => ['nomvendedor','cantidad','precio','comision','beneficio'],
                'defaultOrder' => [
                    'precio' => SORT_DESC,
                    'cantidad' => SORT_DESC,
                    'beneficio' => SORT_DESC,
                ]
            ],
            
        ]);
        return $this->render('mejoresvendedoresevento', [
            'dataProvider' => $dataProvider,
            'total'=>$total,
        ]);
    }
    
    public function actionReport($id){
        /*
         * Redirige a un archivo pdf con los datos de la entrada, evento, cliente, lugar, fecha y hora del evento
         */
        $model = $this->actionRelacionar($id);
	
	$ubicacion = \Yii::$app->db->createCommand('SELECT MIN(CONCAT(recintos.nombre, " - ", recintos.ubicacion, "<br>", alquileres.f_inicio, " - ", h_inicio)) FROM alquileres INNER JOIN recintos ON alquileres.recinto = recintos.id WHERE alquileres.evento = ' . $model->evento)->queryScalar();
        
	$html = $this->renderPartial('_reportView', ['model'=>$model, 'ubicacion'=>$ubicacion]);
        $pdf = \yii::$app->pdf;
        $pdf->cssFile = '@webroot/css/pdf.css';
        $pdf->filename = "Entrada " . $model->evento0->nombre . ".pdf";
        \Yii::$app->response->format = \yii\web\Response::FORMAT_RAW; 
        \Yii::$app->response->headers->add('Content-Type', 'application/pdf');
        $pdf->content = $html;
        
        return $pdf->render();
    }
}
