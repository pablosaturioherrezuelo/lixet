<?php

namespace app\models;

use Yii;
use borales\extensions\phoneInput\PhoneInputValidator;


/**
 * This is the model class for table "clientes".
 *
 * @property int $id
 * @property string|null $email
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $telefono
 * @property string|null $f_nacimiento
 *
 * @property Entradas[] $entradas
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $date;
    //public $date = (new DateInterval('P18Y'));
    //public $max = $date->format('Y-m-d');
    
    
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        // Fecha de hoy
        $date = new \DateTime();
        // Hace 14 años
        $date->sub(new \DateInterval('P14Y'));
        // Fecha maxima de nacimiento
        $max = $date->format('Y-m-d');
        return [
            [['f_nacimiento'], 'safe'],
            [['f_nacimiento'], 'date', 'format' => 'yyyy-M-d', 'max' => $max, 'tooBig' => 'En base a la ley de protección de menores el cliente no puede ser menor de 14 años para los eventos enfocados a menores de edad.'],
            [['email','nombre', 'apellidos'], 'string', 'max' => 80],
            [['nombre','apellidos','f_nacimiento'], 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetAttribute' => 'email'],
            [['telefono'], 'string', 'max' => 13],
            [['telefono'], PhoneInputValidator::className(), 'region' => 'ES'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'telefono' => 'Teléfono',
            'f_nacimiento' => 'Fecha de nacimiento',
        ];
    }

    /**
     * Gets query for [[Entradas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntradas()
    {
        return $this->hasMany(Entradas::className(), ['comprador' => 'id']);
    }
}
