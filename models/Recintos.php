<?php

namespace app\models;

use Yii;
use borales\extensions\phoneInput\PhoneInputValidator;

/**
 * This is the model class for table "recintos".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $ubicacion
 * @property int|null $aforo
 * @property string|null $tipo
 * @property string|null $telefono
 *
 * @property Alquileres[] $alquileres
 * @property Eventos[] $eventos
 */
class Recintos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $evento;
    public static function tableName()
    {
        return 'recintos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['aforo', 'nombre', 'ubicacion'], 'required'],
            [['aforo'], 'integer'],
            [['nombre'], 'string', 'max' => 80],
            [['ubicacion'], 'string', 'max' => 100],
            [['tipo'], 'string', 'max' => 50],
            [['telefono'], 'string', 'max' => 13],
            [['telefono'], PhoneInputValidator::className(), 'region' => 'ES'], //Validacion de numero de telefono con plugin phone imput validator
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'ubicacion' => 'Ubicación',
            'aforo' => 'Aforo',
            'tipo' => 'Tipo',
            'telefono' => 'Teléfono',
        ];
    }

    /**
     * Gets query for [[Alquileres]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlquileres()
    {
        return $this->hasMany(Alquileres::className(), ['recinto' => 'id']);
    }

    /**
     * Gets query for [[Eventos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEventos()
    {
        return $this->hasMany(Eventos::className(), ['id' => 'evento'])->viaTable('alquileres', ['recinto' => 'id']);
    }
    
    public function setEventos($evento) {
        $this->evento = $evento;
    }
}
