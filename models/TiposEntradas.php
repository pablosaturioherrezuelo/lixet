<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tipos_entradas".
 *
 * @property int $id
 * @property int $evento
 * @property int $numero
 * @property string $tipo
 *
 * @property Entradas $evento0
 */
class TiposEntradas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    
    public $nomevento;


    public static function tableName()
    {
        return 'tipos_entradas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['evento', 'numero', 'tipo'], 'required'],
            [['evento', 'numero'], 'integer'],
            [['tipo'], 'string', 'max' => 80],
            [['evento', 'numero', 'tipo'], 'unique', 'targetAttribute' => ['evento', 'numero', 'tipo']],
            [['evento', 'numero'], 'unique', 'targetAttribute' => ['evento', 'numero']],
            [['evento', 'numero'], 'exist', 'skipOnError' => true, 'targetClass' => Entradas::className(), 'targetAttribute' => ['evento' => 'evento', 'numero' => 'numero']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'evento' => 'Evento',
            'numero' => 'Número',
            'tipo' => 'Tipo',
            'nomevento' => 'Evento',
        ];
    }

    /**
     * Gets query for [[Evento0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEvento0()
    {
        return $this->hasOne(Entradas::className(), ['evento' => 'evento', 'numero' => 'numero']);
    }
    
    public function getdropdownEventos(){
        
        /*
         * Devuelve todos los eventos en un menu desplegable utilizado en los formularios
         */
        
        $models = Eventos::find()->asArray()->select(['id, concat(nombre, " (", codigo, "-", edicion, ")") nombre'])->orderBy('id desc')->all();
        return ArrayHelper::map($models, 'id', 'nombre');
       
    }
    
    public function getdropdownNumero(){
        
        /*
         *  NO USAR!!!! Existe un procedimiento de la BBDD que solventa el problema de seleccion de numero
         */
        
        $models = Entradas::find()->asArray();
        return ArrayHelper::map($models, 'id', 'numero');
       
    }
    
}
