<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "jefes".
 *
 * @property int $id
 * @property int $jefe
 * @property int $vendedor
 *
 * @property Rrpps $jefe0
 * @property Rrpps $vendedor0
 */
class Jefes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    
    /*
     * Variables necesarias para campos de tablas relacionadas y calculados
     */
    public $nomjefe;
    public $nomrrpp;
    public $integrantes;
    public $tipo; //Tipo hace referencia a su rol (jefe o vendedor)
    public $telefono;

    public static function tableName()
    {
        return 'jefes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jefe', 'vendedor'], 'required'],
            [['jefe', 'vendedor'], 'integer'],
            [['jefe', 'vendedor'], 'unique', 'targetAttribute' => ['jefe', 'vendedor'], 'message'=>'Ya existe esta combinación de jefe y vendedor.'],
            [['vendedor'], 'unique', 'message'=>'Este {attribute} ya está en un grupo. Para asignarle uno nuevo, eliminelo del anterior primero.'],
            [['vendedor'], 'exist', 'skipOnError' => true, 'targetClass' => Rrpps::className(), 'targetAttribute' => ['vendedor' => 'id']],
            [['jefe'], 'exist', 'skipOnError' => true, 'targetClass' => Rrpps::className(), 'targetAttribute' => ['jefe' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jefe' => 'Jefe',
            'vendedor' => 'Vendedor',
            'nomjefe' => 'Jefe',
            'nomrrpp' => 'RRPP',
            'tipo' => 'Tipo',
            'telefono' => 'Teléfono',
        ];
    }

    /**
     * Gets query for [[Jefe0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJefe0()
    {
        return $this->hasOne(Rrpps::className(), ['id' => 'jefe']);
    }

    /**
     * Gets query for [[Vendedor0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVendedor0()
    {
        return $this->hasOne(Rrpps::className(), ['id' => 'vendedor']);
    }
    
    public function getdropdownRrpps(){
        
        /*
         * Devuelve todos los rrpps a excepcion del -1 que esta reservado para el sistema
         * en un menu desplegable utilizado en los formularios
         */
        
        $models = Rrpps::find()->asArray()->select(['id, concat(nombre, " ", apellidos) nombre'])->where('id <> -1')->orderBy('nombre asc, apellidos asc')->all();
        return ArrayHelper::map($models, 'id', 'nombre');
        
    }
    
    public function getdropdownJefes(){
        
        /*
         * Devuelve todos los jefes (rrpps cuyo jefe es el -1 que hace referencia al sistema)
         * en un menu desplegable utilizado en los formularios
         */
        
        $models = Rrpps::find()->asArray()->select(['rrpps.id, concat(nombre, " ", apellidos) nombre'])->where('jefes.jefe = -1')->innerJoinWith('jefes')->orderBy('nombre asc, apellidos asc')->all();
        return ArrayHelper::map($models, 'id', 'nombre');
        
    }
}
