<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "entradas".
 *
 * @property int $id
 * @property int $evento
 * @property int $numero
 * @property float $precio
 * @property int|null $vendedor
 * @property float|null $comision
 * @property int|null $comprador
 * @property string|null $fecha
 * @property string|null $hora
 *
 * @property Clientes $comprador0
 * @property Eventos $evento0
 * @property TiposEntradas[] $tiposEntradas
 * @property Rrpps $vendedor0
 */
class Entradas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    
    /*
     * Variables necesarias para campos de tablas relacionadas y calculados
     */
    public $nomevento;
    public $tipo;
    public $nomcomprador;
    public $nomvendedor;
    public $numentrada;
    public $rrpp;
    public $codigo;
    public $cantidad;
    public $fech;
    public $beneficio;

    public static function tableName()
    {
        return 'entradas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['evento', 'numero', 'precio','cantidad','tipo'], 'required'],
            [['tipo'], 'string', 'max' => 80],
            [['evento', 'numero', 'vendedor', 'comprador','cantidad'], 'integer'],
            [['precio', 'comision'], 'number', 'min' => 0], //minimo precio
            ['precio', 'compare', 'compareAttribute' => 'comision', 'operator' => '>'], // Compara que el precio no sea menor que la comision
            [['cantidad'],'integer', 'min' => 1], // Cantidad es obligatorio minimo 1 para que funcione el procedimiento (se asigna automaticamente en el controlador)
            [['fecha', 'hora'], 'safe'],
            [['fecha'], 'date', 'format' => 'yyyy-M-d'],
            [['hora'], 'time', 'format' => 'H:m'],
            [['evento', 'numero'], 'unique', 'targetAttribute' => ['evento', 'numero']],
            [['comprador','vendedor'], 'default', 'value' => null],
            [['comprador'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['comprador' => 'id']],
            [['evento'], 'exist', 'skipOnError' => true, 'targetClass' => Eventos::className(), 'targetAttribute' => ['evento' => 'id']],
            [['vendedor'], 'exist', 'skipOnError' => true, 'targetClass' => Rrpps::className(), 'targetAttribute' => ['vendedor' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'evento' => 'Evento',
            'numero' => 'Número',
            'precio' => 'Precio €',
            'vendedor' => 'Vendedor',
            'comision' => 'Comisión €',
            'comprador' => 'Comprador',
            'fecha' => 'Fecha',
            'hora' => 'Hora',
            'nomevento' => 'Evento',
            'tipo' => 'Tipo',
            'nomcomprador' => 'Cliente',
            'nomvendedor' => 'RRPP',
            'numentrada' => 'Número',
            'fech' => 'Fecha',
            'cantidad' => 'Cantidad',
            'beneficio' => 'Beneficio',
        ];
    }

    /**
     * Gets query for [[Comprador0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComprador0()
    {
        return $this->hasOne(Clientes::className(), ['id' => 'comprador']);
    }

    /**
     * Gets query for [[Evento0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEvento0()
    {
        return $this->hasOne(Eventos::className(), ['id' => 'evento']);
    }

    /**
     * Gets query for [[TiposEntradas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTiposEntradas()
    {
        //return $this->hasMany(TiposEntradas::className(), ['evento' => 'evento', 'numero' => 'numero']);
        return $this->hasOne(TiposEntradas::className(), ['evento' => 'evento', 'numero' => 'numero']);
    }

    /**
     * Gets query for [[Vendedor0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVendedor0()
    {
        return $this->hasOne(Rrpps::className(), ['id' => 'vendedor']);
    }
    
    public function getdropdownEventos(){
        
        /*
         * Devuelve todos los eventos en un menu desplegable utilizado en los formularios
         */
        
        $models = Eventos::find()->asArray()->select(['id, concat(nombre, " (", codigo, "-", edicion, ")") nombre'])->orderBy('id desc')->all();
        return ArrayHelper::map($models, 'id', 'nombre');
       
    }
    
    public function getdropdownRrpps(){
        
        /*
         * Devuelve todos los rrpps a excepcion del -1 que esta reservado para el sistema
         * en un menu desplegable utilizado en los formularios
         */
        
        $models = Rrpps::find()->asArray()->select(['id, concat(nombre, " ", apellidos) nombre'])->where('id <> -1')->orderBy('nombre asc, apellidos asc')->all();
        return ArrayHelper::map($models, 'id', 'nombre');
        
    }
    
    public function getdropdownClientes(){
        
        /*
         * Devuelve todos los clientes en un menu desplegable utilizado en los formularios
         */
        
        $models = Clientes::find()->asArray()->select(['id, concat(nombre, " ", apellidos) nombre'])->orderBy('nombre asc, apellidos asc')->all();
        return ArrayHelper::map($models, 'id', 'nombre');
        
    }
}
