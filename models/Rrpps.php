<?php

namespace app\models;

use Yii;
use borales\extensions\phoneInput\PhoneInputValidator;

/**
 * This is the model class for table "rrpps".
 *
 * @property int $id
 * @property string|null $dni
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $telefono
 * @property string|null $email
 * @property string|null $f_nacimiento
 *
 * @property Contratos[] $contratos
 * @property Entradas[] $entradas
 * @property Eventos[] $eventos
 * @property Jefes $jefes
 * @property Jefes[] $jefes0
 * @property Rrpps[] $jeves
 * @property Rrpps[] $vendedors
 */
class Rrpps extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    
    /*
     * Variables necesarias para campos de tablas relacionadas y calculados
     */
    public $numentradas;
    public $evento;
    
    public static function tableName()
    {
        return 'rrpps';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        // Fecha de hoy
        $date = new \DateTime();
        // Hace 14 años
        $date->sub(new \DateInterval('P14Y'));
        // Fecha maxima de nacimiento
        $max = $date->format('Y-m-d');
        return [
            [['f_nacimiento'], 'safe'],
            [['f_nacimiento'], 'date', 'format' => 'yyyy-M-d', 'max' => $max, 'tooBig' => 'En base a la ley de protección de menores el RRPP no puede ser menor de 14 años para los eventos enfocados a menores de edad.'],
            //Validar dni
            [['dni'], 'string', 'max' => 9],
            [['dni'], 'match', 'pattern' => '/^[0-9]{8}[A-Z]{1}/i'], //Permite introducir unicamente 8 numeros y una letra
            [['nombre', 'apellidos', 'email'], 'string', 'max' => 80],
            [['nombre','apellidos','f_nacimiento','telefono','dni'], 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetAttribute' => 'email'],
            [['telefono'], 'string', 'max' => 13],
            [['telefono'], PhoneInputValidator::className(), 'region' => 'ES'], //Validacion de numero de telefono con plugin phone imput validator
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dni' => 'DNI',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'telefono' => 'Teléfono',
            'email' => 'Email',
            'f_nacimiento' => 'Fecha de nacimiento',
            'numentradas' => 'Entradas',
        ];
    }

    /**
     * Gets query for [[Contratos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContratos()
    {
        return $this->hasMany(Contratos::className(), ['rrpp' => 'id']);
    }

    /**
     * Gets query for [[Entradas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntradas()
    {
        return $this->hasMany(Entradas::className(), ['vendedor' => 'id']);
    }

    /**
     * Gets query for [[Eventos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEventos()
    {
        return $this->hasMany(Eventos::className(), ['id' => 'evento'])->viaTable('contratos', ['rrpp' => 'id']);
    }

    /**
     * Gets query for [[Jefes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJefes()
    {
        return $this->hasOne(Jefes::className(), ['vendedor' => 'id']);
    }

    /**
     * Gets query for [[Jefes0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJefes0()
    {
        return $this->hasMany(Jefes::className(), ['jefe' => 'id']);
    }

    /**
     * Gets query for [[Jeves]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJeves()
    {
        return $this->hasMany(Rrpps::className(), ['id' => 'jefe'])->viaTable('jefes', ['vendedor' => 'id']);
    }

    /**
     * Gets query for [[Vendedors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVendedors()
    {
        return $this->hasMany(Rrpps::className(), ['id' => 'vendedor'])->viaTable('jefes', ['jefe' => 'id']);
    }
    
}
