<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\Security;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;
use app\models\User as UserCommon; 

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $authKey
 * @property string $accessToken
 * @property int|null $idrrpp
 * @property int|null $idcliente
 *
 * @property Clientes $idcliente0
 * @property Rrpps $idrrpp0
 */
class User extends ActiveRecord implements IdentityInterface
{
    
    /*
     * Variables necesarias para campos de tablas relacionadas
     */
    public $nomrrpp;
    public $nomcliente;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'email', 'password'/*, 'authKey', 'accessToken'*/], 'required'],
            [['idrrpp', 'idcliente'], 'integer'],
            [['username', 'password', 'email'], 'string', 'max' => 60],
            [['email'], 'email'],
            [['username'], 'match', 'pattern' => '/^[a-z]\w*$/i'], // Permite solo letras y busca coincidencias
            [['password'], 'string', 'min' => 1],
            [['authKey', 'accessToken'], 'string', 'max' => 250],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['idrrpp'], 'unique'],
            [['idcliente'], 'unique'],
            [['idcliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['idcliente' => 'id']],
            [['idrrpp'], 'exist', 'skipOnError' => true, 'targetClass' => Rrpps::className(), 'targetAttribute' => ['idrrpp' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Usuario'),
            'password' => Yii::t('app', 'Contraseña'),
            'email' => Yii::t('app', 'Email'),
            'authKey' => Yii::t('app', 'Auth Key'),
            'accessToken' => Yii::t('app', 'Access Token'),
            'idrrpp' => Yii::t('app', 'RRPP'),
            'idcliente' => Yii::t('app', 'Cliente'),
            'nomrrpp' => 'RRPP',
            'nomcliente' => 'Cliente',
        ];
    }

    /**
     * Gets query for [[Idcliente0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcliente0()
    {
        return $this->hasOne(Clientes::className(), ['id' => 'idcliente']);
    }

    /**
     * Gets query for [[Idrrpp0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdrrpp0()
    {
        return $this->hasOne(Rrpps::className(), ['id' => 'idrrpp']);
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
          return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }
    public function getIdRrpp()
    {
        return $this->idrrpp;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        // Valida el campo authkey con el de la bbdd
        return $this->authKey === $authKey;
    }
    
    public function validatePassword($password)
    {
        // Valida la contraseña introducida con la hasheada en la BBDD
        
        //return $this->password === $password;
        return password_verify($password, $this->password);
    }
    
    public function getdropdownRrpps(){
        
        /*
         * Devuelve todos los rrpps a excepcion del -1 que esta reservado para el sistema
         * en un menu desplegable utilizado en los formularios
         */
        
        $models = Rrpps::find()->asArray()->select(['id, concat(nombre, " ", apellidos) nombre'])->where('id <> -1')->orderBy('nombre asc, apellidos asc')->all();
        return ArrayHelper::map($models, 'id', 'nombre');
        
    }
	
    public function getdropdownClientes(){
        
        /*
         * Devuelve todos los clientes en un menu desplegable utilizado en los formularios
         * Excepto el -1 que en los usuarios esta reservado para el sistma
         */
        
        $models = Clientes::find()->asArray()->select(['id, concat(nombre, " ", apellidos) nombre'])->where('id <> -1')->orderBy('nombre asc, apellidos asc')->all();
        return ArrayHelper::map($models, 'id', 'nombre');
        
    }
    
    /*private function generateAuthKey() {
         $this->authKey = \Yii::$app->security->generateRandomString();
    }*/
    
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        // Cifra la contraseña introducida en el formulario con hash y la sustituye
        //$this->password = Security::generatePasswordHash($password);
        $this->password = password_hash($password, PASSWORD_DEFAULT); 
    }
    
}
