<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "contratos".
 *
 * @property int $id
 * @property int $evento
 * @property int $rrpp
 *
 * @property Eventos $evento0
 * @property Rrpps $rrpp0
 */
class Contratos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    
    /*
     * Variables necesarias para campos de tablas relacionadas
     */
    public $nomrrpp;
    public $nomevento;
    public $codigo;
    
    public static function tableName()
    {
        return 'contratos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['evento', 'rrpp'], 'required'],
            [['evento', 'rrpp'], 'integer'],
            [['evento', 'rrpp'], 'unique', 'targetAttribute' => ['evento', 'rrpp']],
            [['evento'], 'exist', 'skipOnError' => true, 'targetClass' => Eventos::className(), 'targetAttribute' => ['evento' => 'id']],
            [['rrpp'], 'exist', 'skipOnError' => true, 'targetClass' => Rrpps::className(), 'targetAttribute' => ['rrpp' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'evento' => 'Evento',
            'rrpp' => 'RRPP',
            'nomrrpp' => 'RRPP',
            'nomevento' => 'Evento',
        ];
    }

    /**
     * Gets query for [[Evento0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEvento0()
    {
        return $this->hasOne(Eventos::className(), ['id' => 'evento']);
    }

    /**
     * Gets query for [[Rrpp0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRrpp0()
    {
        return $this->hasOne(Rrpps::className(), ['id' => 'rrpp']);
    }
    
    public function getdropdownEventos(){
        
        /*
         * Devuelve todos los eventos en un menu desplegable utilizado en los formularios
         */
        
        $models = Eventos::find()->asArray()->select(['id, concat(nombre, " (", codigo, "-", edicion, ")") nombre'])->orderBy('id desc')->all();
        return ArrayHelper::map($models, 'id', 'nombre');
       
    }
    
    public function getdropdownRrpps(){
        
        /*
         * Devuelve todos los rrpps a excepcion del -1 que esta reservado para el sistema
         * en un menu desplegable utilizado en los formularios
         */
        
        $models = Rrpps::find()->asArray()->select(['id, concat(nombre, " ", apellidos) nombre'])->where('id <> -1')->orderBy('nombre asc, apellidos asc')->all();
        return ArrayHelper::map($models, 'id', 'nombre');
        
    }
}
