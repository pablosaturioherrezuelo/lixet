<?php

namespace app\models;
//use yii\base\model;

/**
 * This is the model class for table "eventos".
 *
 * @property int $id
 * @property string $codigo
 * @property int $edicion
 * @property string|null $nombre
 * @property string|null $tipo
 *
 * @property Alquileres[] $alquileres
 * @property Contratos[] $contratos
 * @property Entradas[] $entradas
 * @property Recintos[] $recintos
 * @property Rrpps[] $rrpps
 */
class Eventos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    
    /*
     * Variables necesarias para campos de tablas relacionadas y calculados
     */
    public $entradas;
    public $rrpps;
    public $file;
    public $filemain;
    public $fecha;
    private $permiso;
    
    public static function tableName()
    {
        return 'eventos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'edicion'], 'required'],
            [['edicion'], 'integer', 'min' => 1],
            [['codigo'], 'string', 'max' => 10],
            [['nombre'], 'string', 'max' => 80],
            [['tipo'], 'string', 'max' => 50],
            [['codigo', 'edicion'], 'unique', 'targetAttribute' => ['codigo', 'edicion']],
            //[['image'], 'safe'],
            //[['image'], 'file', 'extensions'=>'jpg, gif, png'],
            /*
             * Reglas para subir imagenes de portada y cabecera
             */
            [['file','filemain'], 'file', 
                //'skipOnEmpty' => false,
                //'uploadRequired' => 'No has seleccionado ningún archivo', //Error
                'maxSize' => 1024*1024*1, //1 MB
                'tooBig' => 'El tamaño máximo permitido es 1MB', //Error
                'minSize' => 10, //10 Bytes
                'tooSmall' => 'El tamaño mínimo permitido son 10 BYTES', //Error
                'extensions' => 'jpg, png',
                'wrongExtension' => 'El archivo {file} no contiene una extensión permitida {extensions}', //Error
                'maxFiles' => 1,
                'tooMany' => 'El máximo de archivos permitidos son {limit}', //Error
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Código',
            'edicion' => 'Edición',
            'nombre' => 'Nombre',
            'tipo' => 'Tipo',
            'entradas' => 'Entradas',
            'rrpps' => 'RRPPS',
            'file' => 'Foto de Cabecera',
            'filemain' => 'Foto de Portada',
            'fech' => 'Fecha',
        ];
    }

    /**
     * Gets query for [[Alquileres]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlquileres()
    {
        return $this->hasMany(Alquileres::className(), ['evento' => 'id']);
    }

    /**
     * Gets query for [[Contratos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContratos()
    {
        return $this->hasMany(Contratos::className(), ['evento' => 'id']);
    }

    /**
     * Gets query for [[Entradas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntradas()
    {
        return $this->hasMany(Entradas::className(), ['evento' => 'id']);
    }

    /**
     * Gets query for [[Recintos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecintos()
    {
        return $this->hasMany(Recintos::className(), ['id' => 'recinto'])->viaTable('alquileres', ['evento' => 'id']);
    }

    /**
     * Gets query for [[Rrpps]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRrpps()
    {
        return $this->hasMany(Rrpps::className(), ['id' => 'rrpp'])->viaTable('contratos', ['evento' => 'id']);
    }
}
