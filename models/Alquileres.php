<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "alquileres".
 *
 * @property int $id
 * @property int $evento
 * @property int $recinto
 * @property string|null $f_inicio
 * @property string|null $h_inicio
 * @property string|null $f_final
 * @property string|null $h_final
 *
 * @property Eventos $evento0
 * @property Recintos $recinto0
 */
class Alquileres extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    
    /*
     * Variables necesarias para campos de tablas relacionadas y calculados
     */
    public $nomevento, $nomrecinto, $inicio, $final;
    public $ubicacion, $aforo, $tipo, $telefono;
    public static function tableName()
    {
        return 'alquileres';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['evento', 'recinto'], 'required'],
            [['evento', 'recinto'], 'integer'],
            [['f_inicio', 'h_inicio', 'f_final', 'h_final'], 'safe'],
            [['f_inicio', 'f_final'], 'date', 'format' => 'yyyy-M-d'], //arreglar fecha
            [['h_inicio', 'h_final'], 'time', 'format' => 'H:m'],
            ['f_final', 'compare', 'compareAttribute' => 'f_inicio', 'operator' => '>='], // Comprobar que la fecha final sea mayor o igual a la fecha de inicio
            [['evento', 'recinto'], 'unique', 'targetAttribute' => ['evento', 'recinto']],
            [['evento'], 'exist', 'skipOnError' => true, 'targetClass' => Eventos::className(), 'targetAttribute' => ['evento' => 'id']],
            [['recinto'], 'exist', 'skipOnError' => true, 'targetClass' => Recintos::className(), 'targetAttribute' => ['recinto' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'evento' => 'Evento',
            'recinto' => 'Recinto',
            'f_inicio' => 'Fecha Inicio',
            'h_inicio' => 'Hora Inicio',
            'f_final' => 'Fecha Final',
            'h_final' => 'Hora Final',
            'nomevento' => 'Evento',
            'nomrecinto' => 'Recinto',
            'inicio' => 'Inicio',
            'final' => 'Final',
            'telefono' => 'Teléfono',
            'tipo' => 'Tipo recinto',
            'ubicacion' => 'Ubicación',
            'aforo' => 'Aforo',   
        ];
    }

    /**
     * Gets query for [[Evento0]].
     *
     * @return \yii\db\ActiveQuery
     */
    
    public function getEvento0()
    {
        return $this->hasOne(Eventos::className(), ['id' => 'evento']);
    }

    /**
     * Gets query for [[Recinto0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecinto0()
    {
        return $this->hasOne(Recintos::className(), ['id' => 'recinto']);
    }
    
    public function getdropdownRecintos(){
        
        /*
         * Devuelve todos los recintos en un menu desplegable utilizado en los formularios
         */
        
        $models = Recintos::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'nombre');
    
        
    }
    
    public function getdropdownEventos(){
        
        /*
         * Devuelve todos los eventos en un menu desplegable utilizado en los formularios
         */
        
        $models = Eventos::find()->asArray()->select(['id, concat(nombre, " (", codigo, "-", edicion, ")") nombre'])->orderBy('id desc')->all();
        return ArrayHelper::map($models, 'id', 'nombre');
       
    }
}
